import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:launch_review/launch_review.dart';
import 'package:package_info/package_info.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/list_players_in_township_page.dart';
import 'package:cafe_app/list_festivals_in_township_page.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/add_festival_page.dart';
import 'package:cafe_app/dialogs.dart';
import 'package:cafe_app/text_input_alert_dialog.dart';
import 'package:cafe_app/about_page.dart';
import 'package:cafe_app/global_config.dart';

class TownshipHomePage extends StatefulWidget {
  final Township restaurant;

  TownshipHomePage({@required this.restaurant});

  @override
  _TownshipHomePageState createState() =>
    new _TownshipHomePageState(restaurant);
}

class ButtonDesc {
  String title;
  String image;
  VoidCallback onPressed;

  ButtonDesc({this.title, this.onPressed, this.image});
}

Widget _listCategory(String caption) {
  return Padding(
    padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
    child: Text(caption,
      style: TextStyle(
        fontWeight: FontWeight.bold)));
}

class _TownshipHomePageState extends State<TownshipHomePage> {
  Township r;
  bool loading;
  int userCount = 0;
  PackageInfo _package;

  _TownshipHomePageState(this.r);

  void initState() {
    super.initState();
    this.loading = true;

    this._asyncInitState().then((_) => setState(() {
      this.loading = false;
    }));
  }

  Future<void> _asyncInitState() async {
    await this._refreshDatabaseVariables();
    this._package = await PackageInfo.fromPlatform();
  }

  Widget _buttonDescToWidget(ButtonDesc desc) {
    var _image = Image.asset(desc.image, width: 48, height: 48);
    var _paddedImage = Padding(padding: EdgeInsets.symmetric(vertical: 8),
      child: _image);

    var _paddedImageTitle = Padding(padding: EdgeInsets.only(left: 10),
      child: CupertinoButton(child: Text(desc.title),
        onPressed: () => desc.onPressed()
      ));

    var _row = Row(children: [
      _paddedImage, _paddedImageTitle
    ]);

    var _gesture = GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: desc.onPressed,
      child: _row,
    );

    return _gesture;
  }

  Widget _listButtons(BuildContext ctx, List<ButtonDesc> buttons) {
    var arr = buttons.map((bd) {
      return _buttonDescToWidget(bd);
    }).toList();

    var _row = Column(crossAxisAlignment: CrossAxisAlignment.start,
      children: arr);

    var _initialPadding = Padding(padding: EdgeInsets.only(left: 16),
      child: _row);

    return _initialPadding;
  }

  Future<void> _refreshDatabaseVariables() async {
    int count = await DatabaseHelper().countUsersForTownshipId(r.id);
    setState(() {
      this.userCount = count;
    });
  }

  void _listRestaurantPlayers() {
    Navigator.push(context, CupertinoPageRoute(
      builder: (context) =>
        ListPlayersInTownshipPage(restaurant: r),
    )).then((r) => this._refreshDatabaseVariables());
  }

  void _listRestaurantFestivals() {
    Navigator.push(context, CupertinoPageRoute(
      builder: (context) =>
        ListFestivalsInTownshipPage(rid: r.id),
    )).then((r) {
      this._refreshDatabaseVariables();
      this._showVoteDialog();
    });
  }

  void _addUser(String playerName) async {
    var u = User(0, widget.restaurant.id, playerName, 0);
    await DatabaseHelper().saveUser(u);
    await _refreshDatabaseVariables();
  }

  void _addNewUser() async {
    TextInputAlertDialog.show(this.context,
      title: i18n(this.context).mainAddNewPlayer,
      body: i18n(this.context).playerNewNameBody,
      onOK: (String buf) => this._addUser(buf),
      textValidator: (String buf) => buf.trim().length > 0,
      okActionTitle: i18n(this.context).globAdd);
  }

  void _addNewFestival() {
    Navigator.push(context, CupertinoPageRoute(
      builder: (context) =>
        AddFestivalPage(restaurant: this.r),
    )).then((_) {
        this._showVoteDialog();
    });
  }

//  void _showUserCharts() {
//    Navigator.push(context, CupertinoPageRoute(
//      builder: (context) =>
//        SelectChartsPage(set: 'user', rid: this.r.id),
//    ));
//  }

  void _showVoteDialog() {
    if(GlobalConfig().neverShowVoteDialog) {
      print("vote dialog marked as 'never display' in GlobalConfig");
      return;
    }

    if(GlobalConfig().runningInDays < GlobalConfig().voteDialogMinimumDaysFromFirstRun) {
      print("vote dialog shouldn't be displayed yet: app is too fresh");
      return;
    }

    if(GlobalConfig().lastVoteDisplayInDays < GlobalConfig().voteDialogMinimumDaysFromLastDisplay) {
      print("Vote dialog shouldn't be displayed yet: last display time is too recent.");
      print("LastVoteDisplayInDays: ${GlobalConfig().lastVoteDisplayInDays}");
      print("Threshold:             ${GlobalConfig().voteDialogMinimumDaysFromLastDisplay}");
      print("LastVoteDisplay:       ${GlobalConfig().lastVoteDisplay}");
      return;
    }

    GlobalConfig().lastVoteDisplay = DateTime.now();
    GlobalConfig().lastVersionCode = int.parse(this._package.buildNumber);

    UserDialogs.yesNoDialog(context,
        title: i18n(context).globQuestion,
        message: i18n(context).globVoteDialogQuestion,
        yesHandler: this._voteYesHandler,
        noHandler: this._voteNoHandler,
        yesLabel: i18n(context).voteYes,
        noLabel: i18n(context).voteNo);
  }

  void _voteYesHandler() {
    UserDialogs.alertYesNoDialog(context,
        title: i18n(context).globThankYou,
        message: i18n(context).globVoteYesAnswer,
        yesHandler: () {
          GlobalConfig().neverShowVoteDialog = true;
          LaunchReview.launch();
        },
        noHandler: () {},
        yesLabel: i18n(context).voteRateIt,
        noLabel: i18n(context).globDoNothing);
  }

  void _voteNoHandler() {
    UserDialogs.yesNoDialog(context,
        title: i18n(context).globSorry,
        message: i18n(context).globVoteNoAnswer,
        yesHandler: () {
          GlobalConfig().neverShowVoteDialog = true;
          launch("mailto:ga@anadoxin.org?subject=${_buildEMailSubject()}&body=${_buildEMailBody()}");
        },
        noHandler: () {},
        yesLabel: i18n(context).voteEMail,
        noLabel: i18n(context).globDoNothing);
  }

  String _buildEMailSubject() {
    return Uri.encodeFull(i18n(context).emailFeedbackSubject);
  }

  String _buildEMailBody() {
    var body = i18n(context).emailNegativeFeedbackBody;

    if(!body.endsWith("\n"))
      body += "\n";

    if(!body.endsWith("\n\n"))
      body += "\n";

    return Uri.encodeFull(body);
  }

  Widget _buildBody() {
    final BuildContext ctx = this.context;

    var festivalsArr = [
      _listCategory(i18n(ctx).mainFestivals),
      _listButtons(ctx, [
        ButtonDesc(title: i18n(ctx).mainNewFestival, image: "assets/add_festival.png", onPressed: this._addNewFestival),
        ButtonDesc(title: i18n(ctx).mainFestivalHistory, image: "assets/edit_festivals.png", onPressed: this._listRestaurantFestivals),
      ]),
    ];

    var playersArr = [
      _listCategory(i18n(ctx).mainPlayers),
      _listButtons(ctx, [
        ButtonDesc(title: i18n(ctx).mainAddNewPlayer, image: "assets/add_user.png", onPressed: this._addNewUser),
        ButtonDesc(title: i18n(ctx).mainManagePlayers, image: "assets/edit_users.png", onPressed: this._listRestaurantPlayers),
      ]),
    ];

//    var chartsArr = [
//      _listCategory("Charts"),
//      _listButtons(ctx, [
//        ButtonDesc(title: "User charts", image: "assets/user_charts.png", onPressed: this._showUserCharts),
//        ButtonDesc(title: "Festival charts", image: "assets/festival_charts.png"),
//      ]),
//    ];

    var managementArr = [
      _listCategory(i18n(ctx).mainManagement),
      _listButtons(ctx, [
        ButtonDesc(title: i18n(ctx).mainAbout, image: "assets/about.png", onPressed: this._about),
        ButtonDesc(title: i18n(ctx).mainRemoveTownshipTitle, image: "assets/purge.png", onPressed: this._removeMyself),
      ])
    ];

    var listArr = List<Widget>();

    if(this.userCount > 0)
      listArr += festivalsArr;

    listArr += playersArr;

//    if(this.userCount > 0)
//      listArr += chartsArr;

    listArr += managementArr;

    return ListView(children: <Widget>[
      Padding(padding:
          EdgeInsets.fromLTRB(12, 12, 12, 0),
          child: Text(i18n(this.context).mainSelectOption)),
    ] + listArr);
  }

  void _doRemoveMyself() async {
    await DatabaseHelper().removeTownshipById(this.r.id);
    Navigator.of(this.context).pop();
  }

  void _about() {
    Navigator.push(this.context, CupertinoPageRoute(
      builder: (ctx) => AboutPage(r: this.r)));
  }

  void _removeMyself() {
    UserDialogs.alertYesNoDialog(this.context,
      yesHandler: _doRemoveMyself,
      noHandler: () {},
      yesLabel: i18n(this.context).globRemove,
      noLabel: i18n(this.context).globDoNothing,
      title: i18n(this.context).globWarning,
      message: i18n(this.context).mainRemoveTownshipBody);
  }

  @override
  Widget build(BuildContext context) {
    var _navigationBar = CupertinoNavigationBar(
      middle: Text(r.name),
    );

    return CupertinoPageScaffold(
      navigationBar: _navigationBar,
      child: SafeArea(child: _buildBody()),
    );
  }
}
