import 'package:flutter/cupertino.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:cafe_app/my_townships_page.dart';
import 'package:cafe_app/add_township_page.dart';
import 'package:cafe_app/debug_page.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/cupertino_strings.dart';

void main() {
  runApp(new MyApp());
}

final routes = {
  "/": (BuildContext context) => MyTownshipsPage(title: i18n(context).townList),
  "/addrest": (BuildContext context) => AddTownshipPage(),
  "/debug": (BuildContext context) => DebugPage(),
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new CupertinoApp(
      localizationsDelegates: [
        StringLocalizationsDelegate(),
        LocalCupertinoLocalizationsDelegate(),
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        // Default locale, leave EN on the beginning!
        const Locale("en", ""),
        // ----
        // Optional locales:
        const Locale("pl", ""),
        const Locale("es", ""),
      ],
      title: 'CafeApp',
      routes: routes,
    );
  }
}
