import 'package:cafe_app/configs/config_loader.dart';
import 'package:cafe_app/global_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:cafe_app/database_helper.dart';

class DebugPage extends StatefulWidget {
  @override
  _DebugPageState createState() => _DebugPageState();
}

class _DebugPageState extends State<DebugPage> {
  void _clearDatabase() {
    var _alert = CupertinoAlertDialog(
      title: Text("Message"),
      content: Text("Database was purged. Please restart the application."),
      actions: <Widget>[
        CupertinoDialogAction(
          child: Text("Close"),
          isDefaultAction: true,
          onPressed: () {
            Navigator.of(context).pop();
          }
        )
      ]
    );

    DatabaseHelper().clearDatabase().then((r) {
      showCupertinoDialog(
        context: context,
        builder: (ctx) { return _alert; });
    });
  }

  Future<void> _setInstalled1M() async {
    print("set installed 1m ago");

    var firstRunConfig = (await ConfigLoader.getFirstRunConfig()).asValue.value;
    firstRunConfig.firstRunDate = DateTime.now().subtract(Duration(days: 60));

    var voteDialogConfig = (await ConfigLoader.getVoteDialogConfig()).asValue.value;
    voteDialogConfig.lastDisplayDate = firstRunConfig.firstRunDate;

    print("Saved first install & dialog shown dates -- there are both ${firstRunConfig.firstRunDate} now.");

    await ConfigLoader.saveFirstRunConfig(firstRunConfig);
    await ConfigLoader.saveVoteDialogConfig(voteDialogConfig);
    await this._reloadConfigs();
  }

  Future<void> _clearConfigs() async {
  }

  Future<void> _reloadConfigs() async {
    await GlobalConfig().forceLoad();
  }

  Future<void> _clearNeverDisplay() async {
    var voteDialogConfig = (await ConfigLoader.getVoteDialogConfig()).asValue.value;
    voteDialogConfig.neverDisplay = false;
    ConfigLoader.saveVoteDialogConfig(voteDialogConfig);
    await this._reloadConfigs();
  }

  Widget _button(String text, VoidCallback fn) {
    var _button = CupertinoButton(
        color: CupertinoColors.destructiveRed,
        child: Text(text),
        onPressed: () {
          print("callback");
          fn();
        },
    );

    var _buttonPadding = Padding(
        padding: EdgeInsets.all(6),
        child: _button);

    return _buttonPadding;
  }

  Widget _buildBody() {
    var _clearDatabaseButton = this._button("Clear database", this._clearDatabase);
    var _reloadConfigs = this._button("Reload configs", this._reloadConfigs);
    var _clearConfigs = this._button("Clear configs", this._clearConfigs);
    var _clearNeverDisplayFlag = this._button("Clear 'never display' flag", this._clearNeverDisplay);
    var _setInstalledAMonthAgo = this._button("Set installed 2 months ago", this._setInstalled1M);

    var _row = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(children: [
          _clearDatabaseButton, _reloadConfigs, _clearConfigs, _clearNeverDisplayFlag, _setInstalledAMonthAgo,
        ])
      ]
    );

    return _row;
  }

  @override
  Widget build(BuildContext context) {
    var _navigationBar = CupertinoNavigationBar(
      middle: Text("Debug screen")
    );

    var _scaffold = CupertinoPageScaffold(
      navigationBar: _navigationBar,
      child: SafeArea(child: _buildBody()),
    );

    return _scaffold;
  }
}
