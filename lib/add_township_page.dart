import 'package:flutter/cupertino.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/township.dart';

class AddTownshipPage extends StatefulWidget {
  @override
  _AddTownshipPageState createState() => _AddTownshipPageState();
}

class _AddTownshipPageState extends State<AddTownshipPage> {
  bool _formValid;
  String _restaurantName;
  TextEditingController _textCtrl;

  Future<void> _storeRestaurant() async {
    final String name = this._restaurantName;
    await DatabaseHelper().saveTownship(Township(0, name));
  }

  void _addRestaurant() {
    _storeRestaurant().then((_) {
      Navigator.pop(context, 1);
    });
  }

  @override
  void initState() {
    super.initState();
    this._formValid = false;
    this._textCtrl = TextEditingController(text: "");
  }

  Widget _buildBody() {
    var _textRow1 = Text(i18n(this.context).townAddBody,
      textAlign: TextAlign.start);

    var _formRow1 = CupertinoTextField(
      autofocus: true,
      controller: this._textCtrl,
      onChanged: (String msg) {
        setState(() {
          this._formValid = msg.trim().length > 0;
          this._restaurantName = msg;
        });
      }
    );

    var _cols = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(padding:
          EdgeInsets.all(8),
          child: _textRow1),

        Padding(
          padding: EdgeInsets.all(8),
          child: _formRow1)
      ]
    );

    var _padding = Padding(
      padding: EdgeInsets.all(8),
      child: _cols);

    return _padding;
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Text(i18n(this.context).globAdd),
      onPressed: this._formValid ? () => this._addRestaurant() : null,
    );

    var _navBar = CupertinoNavigationBar(
      middle: Text(i18n(this.context).townAddTitle),
      trailing: _addButton,
    );

    var _scaffold = CupertinoPageScaffold(
      navigationBar: _navBar,
      child: SafeArea(child: _buildBody()),
    );

    return _scaffold;
  }
}
