import 'package:cafe_app/strings.dart';
import 'package:flutter/material.dart';

class FullscreenMessagePage extends StatefulWidget {
  FullscreenMessagePage({Key key, this.message, this.title}) : super(key: key);

  final String message;
  final String title;

  @override
  _FullscreenMessagePageState createState() => _FullscreenMessagePageState();
}

class _FullscreenMessagePageState extends State<FullscreenMessagePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 16.0),
              child: SizedBox(
                width: 200.0,
                height: 100.0,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: const Color(0x00000000),
                    image: DecorationImage(
                      image: ExactAssetImage('assets/mycafe-logo.png'),
                      fit: BoxFit.contain,
                    ))))),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(widget.title ?? i18n(this.context).globHello,
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                )
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(32, 8, 32, 0),
              child: Text(widget.message ?? "...",
                textAlign: TextAlign.center)
            )
          ]
        )
      )
    );
  }
}
