import 'package:flutter/cupertino.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/dismiss.dart';
import 'package:cafe_app/list_view_item.dart';
import 'package:cafe_app/text_input_alert_dialog.dart';

class UserWithPoints {
  User _u;
  int _points;

  UserWithPoints(this._u, this._points);

  get user => _u;
  get points => _points;
}

class ListPlayersInTownshipPage extends StatefulWidget {
  final Township restaurant;

  ListPlayersInTownshipPage({Key key, this.restaurant}) : super(key: key);

  @override
  _ListPlayersInTownshipPageState createState() => _ListPlayersInTownshipPageState();
}

class _ListPlayersInTownshipPageState extends State<ListPlayersInTownshipPage> {
  var _userList = new List<User>();

  @override
  void initState() {
    super.initState();
    _refreshPlayerList();
  }

  Widget _buildFirstItem() {
    var _tile = ListViewItem(
      title: i18n(this.context).playerAddNew,
      icon: Icon(CupertinoIcons.add),
      onTap: () => _showAddNewUserDialog(),
    );

    var _paddedTile = Padding(padding: EdgeInsets.only(left: 16, top: 8),
      child: _tile);

    return _paddedTile;
  }

  void _refreshPlayerList() {
    DatabaseHelper().loadActiveUsersForTownship(widget.restaurant).then((ulist) {
      setState(() { _userList = ulist; });
    });
  }

  void _addUser(String name) {
    var user = User(0, widget.restaurant.id, name, 0);
    DatabaseHelper().saveUser(user).then((_) {
      _refreshPlayerList();
    });
  }

  void _showAddNewUserDialog() {
    TextInputAlertDialog.show(this.context,
      body: i18n(this.context).playerNewNameBody,
      title: i18n(this.context).playerAdd,
      onOK: (name) => _addUser(name),
      textValidator: (buf) => buf.trim().length > 0,
      okActionTitle: i18n(this.context).globAdd,
    );
  }

  void _renamePlayerDialog(User u) {
    TextInputAlertDialog.show(this.context,
      body: i18n(this.context).playerRenameBody,
      title: i18n(this.context).playerRenameTitle,
      initialText: u.name,
      onOK: (name) {
        DatabaseHelper().renameUserById(u.id, name).then((f) {
          this._refreshPlayerList();
        });
      },
      textValidator: (buf) => buf.trim().length > 0,
      okActionTitle: i18n(this.context).playerRename,
    );
  }

  List<Widget> _buildRestItems() {
    return _userList.map((User u) {
      var tile = ListViewItem(
        icon: Icon(CupertinoIcons.person_solid),
        title: u.name,
        onLongTap: () => this._renamePlayerDialog(u),
      );

      var paddedTile = Padding(padding: EdgeInsets.only(left: 16),
        child: tile);

      var dismissibleObject = Dismissible(
        key: Key("user-${u.id}"),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {
          DatabaseHelper().disableUserById(u.id);
        },
        background: dismissRemoveContainer(this.context),
        child: paddedTile);

      return dismissibleObject;
    }).toList();
  }

  Widget _buildComment() {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Text(i18n(this.context).playerInstructionText));
  }

  List<Widget> _buildListViewItems() {
    return [_buildComment()] + [_buildFirstItem()] + _buildRestItems();
  }

  Widget _buildBody() {
    return new Container(
      child: ListView(
        children: _buildListViewItems(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Text(i18n(this.context).playerAdd),
      onPressed: () => _showAddNewUserDialog());

    var _navBar = CupertinoNavigationBar(
      previousPageTitle: this.widget.restaurant.name,
      middle: Text(i18n(this.context).mainPlayers),
      trailing: _addButton,
    );

    return CupertinoPageScaffold(
      navigationBar: _navBar,
      child: _buildBody(),
    );
  }
}
