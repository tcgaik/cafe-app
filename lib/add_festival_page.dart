import 'package:flutter/cupertino.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/edit_festival_page.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/property_list_view.dart';

class AddFestivalPage extends StatefulWidget {
  final Township restaurant;
  final String prevPageTitle;

  AddFestivalPage({this.restaurant, this.prevPageTitle});

  @override
  _AddFestivalPageState createState() =>
    _AddFestivalPageState(restaurant);
}

class _AddFestivalPageState extends State<AddFestivalPage> {
  Township r;
  DateTime selectedDate = DateTime.now();
  int _yellow = 0, _red = 0, _blue = 0, _tasks = 0;
  bool _ffModeFlag = false;

  _AddFestivalPageState(this.r);

  @override
  void initState() {
    super.initState();
    this._yellow = 0;
    this._red = 0;
    this._blue = 0;
    this._tasks = 0;
  }

  void _addFestival() {
    var db = DatabaseHelper();
    var f = Festival(0, r.id, "", this.selectedDate.millisecondsSinceEpoch, this._yellow, this._red, this._blue, this._tasks);
    db.addFestival(f).then((int id) {
      var editFestivalRoute = CupertinoPageRoute(builder: (context) =>
        EditFestivalPage(fid: id));

      Navigator.push(context, editFestivalRoute).then((result) {
        Navigator.of(context).pop();
      });
    });
  }

  bool _validateForm() {
    var flag = this.selectedDate != null && this._yellow > 0 && (this._red > 0 || this._blue > 0);

    if(this._ffModeFlag) {
      if(this._tasks == 0)
        return false;
    }

    return flag;
  }

  Widget _buildPage() {
    return PropertyListView(
      items: [
        LinePropertyWidget.from(i18n(this.context).festChoose),

        BooleanPropertyWidget(name: i18n(this.context).festUseQuests, defaultValue: this._ffModeFlag,
          setCallback: (bool v) => setState(() => this._ffModeFlag = v),
          getCallback: () => this._ffModeFlag),

        LinePropertyWidget.from(i18n(this.context).festChooseProperties),

        DatePropertyWidget.from(i18n(this.context).festDate, selectedDate,
          setCallback: (newDate) => setState(() { this.selectedDate = newDate; })),

        NumericPropertyWidget.from(i18n(this.context).globTrophyPoints, _yellow, suffix: i18n(this.context).globPointSuffix,
          editTitle: i18n(this.context).festTrophyCount,
          editBody: i18n(this.context).festTrophiesReceivedTitle,
          setCallback: (value) => setState(() { this._yellow = value; })),

        this._ffModeFlag ?
          NumericPropertyWidget.from(i18n(this.context).festQuests, this._tasks, suffix: i18n(this.context).globQuestSuffix,
            editTitle: i18n(this.context).festQuestCount,
            editBody: i18n(this.context).festQuestReceivedTitle,
            setCallback: (value) => setState(() => this._tasks = value)) :
          null,

        NumericPropertyWidget.from(i18n(this.context).festDiamonds, this._blue, suffix: i18n(this.context).globDiamondSuffix,
          editTitle: i18n(this.context).festDiamondCount,
          editBody: i18n(this.context).festDiamondReceivedTitle,
          setCallback: (value) => setState(() { this._blue = value; })),

        NumericPropertyWidget.from(i18n(this.context).festRubies, this._red, suffix: i18n(this.context).globRubySuffix,
          editTitle: i18n(this.context).festRubyCount,
          editBody: i18n(this.context).festRubyReceivedTitle,
          setCallback: (value) => setState(() { this._red = value; })),
      ]
    );
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
        padding: EdgeInsets.all(0),
        child: Text(i18n(this.context).globCreate),
        onPressed: this._validateForm() ? _addFestival : null);

    var _navBar = CupertinoNavigationBar(
      middle: Text(i18n(this.context).mainNewFestival),
      previousPageTitle: this.widget.prevPageTitle == null ? this.widget.restaurant.name : this.widget.prevPageTitle,
      trailing: _addButton,
    );

    return CupertinoPageScaffold(
      navigationBar: _navBar,
      child: _buildPage(),
    );
  }
}
