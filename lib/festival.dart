class Festival {
  String _name;
  int _id;
  int _restaurantId;
  int _dateMSSinceEpoch;
  int _yellow;
  int _red;
  int _blue;
  int _tasks;
  int _method = 1;

  Festival(this._id, this._restaurantId, this._name, this._dateMSSinceEpoch, this._yellow, this._red, this._blue, this._tasks);

  Festival.map(dynamic object) {
    this._id = object['id'];
    this._name = object['name'];
    this._restaurantId = object['rid'];
    this._dateMSSinceEpoch = object['date'];
    this._yellow = object['yellow'];
    this._red = object['red'];
    this._blue = object['blue'];
    this._tasks = object['tasks'];
    this._method = object['method'];
  }

  String get name => this._name;
  int get id => this._id;
  int get rid => this._restaurantId;
  int get restaurantId => this._restaurantId;
  int get dateMSSinceEpoch => this._dateMSSinceEpoch;
  int get date => this._dateMSSinceEpoch;
  int get yellow => this._yellow;
  int get trophies => this._yellow;
  int get red => this._red;
  int get blue => this._blue;
  int get tasks =>  this._tasks;
  int get method => this._method;
  DateTime get dateTime => DateTime.fromMillisecondsSinceEpoch(this._dateMSSinceEpoch);

  Map<String, dynamic> toMap() {
    var m = Map<String, dynamic>();
    m['id'] = this._id;
    m['name'] = this._name;
    m['rid'] = this._restaurantId;
    m['date'] = this._dateMSSinceEpoch;
    m['yellow'] = this._yellow;
    m['red'] = this._red;
    m['blue'] = this._blue;
    m['tasks'] = this._tasks;
    m['method'] = this._method;
    return m;
  }

  Map<String, dynamic> toMapNoId() {
    var m = this.toMap();
    m.remove('id');
    return m;
  }
}
