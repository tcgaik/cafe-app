import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cafe_app/add_player_to_festival.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/score.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/loading_page.dart';

class FullScore {
  Score score;
  int red, blue;

  FullScore(this.score, this.red, this.blue);
}

class EditFestivalPage extends StatefulWidget {
  final int fid;
  EditFestivalPage({this.fid});

  @override
  _EditFestivalPageState createState() =>
    new _EditFestivalPageState(fid);
}

class _EditFestivalPageState extends State<EditFestivalPage> {
  final int fid;
  Festival f;
  Township r;
  int yellowLeft;
  int blueLeft;
  int redLeft;
  int tasksLeft;
  int _selectedMethod = 2;
  bool _ffFlag = false;
  List<Score> scoreList;
  List<FullScore> fullScoreList;
  var selectionMap = Map<int, bool>(); // key: scoreId
  var usersInThisRestaurant = Map<int, User>();
  var df = DateFormat("d MMMM yyyy (EEEE)");

  _EditFestivalPageState(this.fid);

  Future<List<FullScore>> _recalculateXY(List<Score> scoreList, int x, int y) async {
    var lst = List<FullScore>();

    double dx = x * 1.0 / 100.0;
    double dy = y * 1.0 / 100.0;

    for(Score s in scoreList) {
      int red, blue;

      red = ((((s.yellow * 1.0) / this.f.yellow) * (this.f.red * dx)) +
        (((s.tasks * 1.0) / this.f.tasks) * (this.f.red * dy))).floor().toInt();

      blue = ((((s.yellow * 1.0) / this.f.yellow) * (this.f.blue * dx)) +
        (((s.tasks * 1.0) / this.f.tasks) * (this.f.blue * dy))).floor().toInt();

      lst.add(FullScore(s, red, blue));
    }

    lst.sort((b, a) => a.score.tasks - b.score.tasks);
    lst.sort((b, a) => a.score.yellow - b.score.yellow);
    lst.sort((b, a) => (a.red + a.blue) - (b.red + b.blue));

    return lst;
  }

  Future<List<FullScore>> _recalculateNormalTrophies(List<Score> scoreList) async {
    var lst = List<FullScore>();

    for(Score s in scoreList) {
      int red, blue;

      red = (((s.yellow * 1.0) / this.f.yellow) * this.f.red).floor().toInt();
      blue = (((s.yellow * 1.0) / this.f.yellow) * this.f.blue).floor().toInt();

      lst.add(FullScore(s, red, blue));
    }

    lst.sort((b, a) => a.score.yellow - b.score.yellow);
    lst.sort((b, a) => (a.red + a.blue) - (b.red + b.blue));

    return lst;
  }

  Future<List<FullScore>> _recalculateFullScoreList(List<Score> scoreList) async {
    switch(this._selectedMethod) {
      // Used when the user didn't check 'enter task count' option on the
      // festival creation page.
      case 0: return this._recalculateNormalTrophies(scoreList);

      // Used when the user did check 'enter task count' on the festival
      // creation page.
      case 1: return this._recalculateNormalTrophies(scoreList);
      case 2: return this._recalculateXY(scoreList, 50, 50);
      case 3: return this._recalculateXY(scoreList, 75, 25);
      default:
        return [];
    }
  }

  Future<void> initMetadata() async {
    this.f = await DatabaseHelper().getFestivalById(this.fid);
    this.r = await DatabaseHelper().getTownshipById(this.f.restaurantId);

    for(User user in await DatabaseHelper().loadAllUsersForTownshipId(this.r.id)) {
      this.usersInThisRestaurant[user.id] = user;
    }

    this.yellowLeft = this.f.yellow;
    this.redLeft = this.f.red;
    this.blueLeft = this.f.blue;
    this.tasksLeft = this.f.tasks;
  }

  Future<void> _refreshSelectionMap() async {
    var _map = await DatabaseHelper().loadSelectionMapForFestivalId(this.fid);
    setState(() {
      this.selectionMap = _map;
    });
  }

  Future<void> _refreshScoreList() async {
    DatabaseHelper().getScoreListByFestivalId(this.fid).then((scoreList) async {
      await this._refreshSelectionMap();
      var fullScore = await this._recalculateFullScoreList(scoreList);

      int yellowSum = fullScore.fold(0, (x, fs) => x + fs.score.yellow);
      int tasksSum = fullScore.fold(0, (x, fs) => x + fs.score.tasks);
      int redSum = fullScore.fold(0, (x, fs) => x + fs.red);
      int blueSum = fullScore.fold(0, (x, fs) => x + fs.blue);

      setState(() {
        this.scoreList = scoreList;
        this.fullScoreList = fullScore;
        this.yellowLeft = this.f.yellow - yellowSum;
        this.tasksLeft = this.f.tasks - tasksSum;
        this.redLeft = this.f.red - redSum;
        this.blueLeft = this.f.blue - blueSum;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    this.initMetadata().then((ret) {
      this._refreshScoreList();
      setState(() {
        this._selectedMethod = this.f.method;
        this._ffFlag = (this._selectedMethod == 2 || this._selectedMethod == 3);
      });
    });
  }

  Future<void> _setFestivalMethod(int method) async {
    await DatabaseHelper().setFestivalMethod(this.fid, method);
  }

  Widget _box(Widget text) {
    var _container = Container(
      //color: CupertinoColors.lightBackgroundGray,
      child: Padding(
        padding: EdgeInsets.all(12),
        child: text
        )
      );

    return _container;
  }

  List<Widget> _buildFestivalHeader() {
    var _textStyle = TextStyle(
      fontSize: 11,
    );

    var _boxTrophies = Column(children: [
      Icon(FontAwesomeIcons.trophy, color: const Color(0xffaaaa00)),
      Padding(
        padding: EdgeInsets.only(top: 10),
        child: Text("${this.f.yellow} ${i18n(this.context).globTrophySuffix(this.f.yellow)}",
          style: _textStyle,
          textAlign: TextAlign.center)),
    ]);

    var _boxRubies = Column(children: [
      Icon(FontAwesomeIcons.gem, color: const Color(0xffff0000)),
      Padding(
        padding: EdgeInsets.only(top: 10),
        child: Text("${this.f.red} ${i18n(this.context).globRubySuffix(this.f.red)}",
          style: _textStyle,
          textAlign: TextAlign.center)),
    ]);

    var _boxDiamonds = Column(children: [
      Icon(FontAwesomeIcons.gem, color: const Color(0xff0000ff)),
      Padding(
        padding: EdgeInsets.only(top: 10),
        child: Text("${this.f.blue} ${i18n(this.context).globDiamondSuffix(this.f.blue)}",
          style: _textStyle,
          textAlign: TextAlign.center)),
    ]);

    var _boxTasks = Column(children: [
      Icon(FontAwesomeIcons.stickyNote, color: const Color(0xffaaaa00)),
      Padding(
        padding: EdgeInsets.only(top: 10),
        child: Text("${this.f.tasks} ${i18n(this.context).globQuestSuffix(this.f.tasks)}",
          style: _textStyle,
          textAlign: TextAlign.center)),
    ]);

    // Boxes with points.
    var _boxRow = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        this._box(_boxTrophies),
        this.f.tasks > 0 ? this._box(_boxTasks) : null,
        this._box(_boxDiamonds),
        this._box(_boxRubies),
      ].where((o) => o != null).toList()
    );

    var _paddedBoxRow = Padding(
      padding: EdgeInsets.only(top: 0),
      child: _boxRow);

//    var _headerText = Padding(padding: EdgeInsets.only(top: 8, bottom: 8),
//      child: Text(i18n(this.context).distributionGoal, textAlign: TextAlign.center));

    List<Widget> chooser;
    if(this.f.tasks > 0) {
      chooser = [Padding(padding: EdgeInsets.only(bottom: 0), child: CupertinoSegmentedControl(
          onValueChanged: (value) {
            setState(() {
              _selectedMethod = value;
              _ffFlag = (value == 2 || value == 3);
            });
            _refreshScoreList();
            _setFestivalMethod(value);
          },
          groupValue: _selectedMethod,
          children: {
            1: Text(i18n(this.context).festOnlyTrophies),
            2: Text("50/50"),
            3: Text("75/25"),
          }
      ))].map((w) { return Row(children: [Expanded(child: w)]); }).toList();
    } else {
      chooser = [];
    }

    return <Widget>[
      _paddedBoxRow
    ] + chooser;
  }

  Widget _padded(double left, double right, Widget w) {
    return Padding(padding: EdgeInsets.fromLTRB(left, 0, right, 0),
      child: w);
  }

  Widget _paddedAll(double l, double t, double r, double b, Widget w) {
    return Padding(padding: EdgeInsets.fromLTRB(l, t, r, b), child: w);
  }

  Widget _row(List<Widget> children) {
    return Row(children: children);
  }

  Widget _expanded(Widget w) {
    return Expanded(child: w);
  }

  List<Widget> _buildUserScoreList() {
    if(this.scoreList == null || this.scoreList.length == 0) {
      // If there are no players added yet, hint users what they have to do.
      var _addNewUsers = Text(i18n(this.context).festNoPlayersAddedYet, textAlign: TextAlign.center);
      var _addNewUsersBox = Padding(padding: EdgeInsets.all(32), child: _addNewUsers);
      var _addNewUsersCenter = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [ _addNewUsersBox ]);

      return <Widget>[ _addNewUsersCenter ];
    }

    var lst = List<Widget>();
    var _headerText = Padding(padding: EdgeInsets.only(top: 16),
        child: Text(i18n(this.context).festPlayerList, textAlign: TextAlign.center));

    lst.add(_headerText);

    int position = 0;
    for(FullScore fScore in this.fullScoreList) {
      Score score = fScore.score;
      int blue = fScore.blue, red = fScore.red;
      var user = this.usersInThisRestaurant[score.uid];

      position++;

      var disabled = "";
      if(user.disabled) {
        disabled = " " + i18n(this.context).festPlayerRemoved;
      }

      TextDecoration decoration;
      Color color, blueColor, redColor;
      bool selected = this.selectionMap[score.id] == true;

      if(selected) {
        decoration = TextDecoration.lineThrough;
        color = CupertinoColors.inactiveGray;
        redColor = color;
        blueColor = color;
      } else {
        decoration = TextDecoration.none;
        color = CupertinoColors.black;
        redColor = CupertinoColors.destructiveRed;
        blueColor = CupertinoColors.activeBlue;
      }

      var obj = _paddedAll(16, 24, 8, 24, _row([
        //Icon(FontAwesomeIcons.gift, color: color, size: 12),
        Text("$position.", style: TextStyle(fontSize: 14)),
        Expanded(child: Padding(
          padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Text("${user.name}$disabled",
            style: TextStyle(
              fontSize: 14,
              color: color,
              decoration: decoration,
            )))),
        _padded(12, 0, _row([
          Icon(FontAwesomeIcons.trophy, color: color, size: 14),
          _padded(6, 8, Text("${score.yellow}", style: TextStyle(color: color))),
          this._ffFlag ? Icon(FontAwesomeIcons.stickyNote, color: color, size: 14) : null,
          this._ffFlag ? _padded(6, 8, Text("${score.tasks}", style: TextStyle(color: color))) : null,
          Icon(FontAwesomeIcons.gem, color: blueColor, size: 14),
          _padded(6, 8, Text("$blue", style: TextStyle(color: blueColor))),
          Icon(FontAwesomeIcons.gem, color: redColor, size: 14),
          _padded(6, 8, Text("$red", style: TextStyle(color: redColor))),
        ].where((o) => o != null).toList())),
      ]));

      var dism = Dismissible(
        key: Key("${score.id}"),
        direction: DismissDirection.endToStart,
        onDismissed: (direction) {
          DatabaseHelper().removeScoreById(score.id).then((_) {
            this._refreshScoreList();
          });
        },
        background: Container(
          color: CupertinoColors.destructiveRed,
          alignment: Alignment.centerRight,
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
            child: Row(children: [
              Expanded(child: Text("")),
              Padding(padding: EdgeInsets.fromLTRB(0, 0, 8, 0), child: Text(i18n(this.context).globRemove, style: TextStyle(color: CupertinoColors.white))),
              Icon(CupertinoIcons.delete, color: CupertinoColors.white),
            ])
          )
        ),
        child: obj);

      var _gesture = GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () { this._hitScore(score); },
        child: dism,
      );

      lst.add(_gesture);
    }

    lst.add(_paddedAll(6, 24, 8, 24, _row([
      _expanded(_padded(12, 12, Text(i18n(this.context).festSummary))),
      Icon(FontAwesomeIcons.trophy),
      _padded(12, 0, _row([
        _padded(8, 8, Text("${this.yellowLeft}")),
        _padded(8, 8, Text("${this.blueLeft}", style: TextStyle(color: CupertinoColors.activeBlue))),
        _padded(8, 8, Text("${this.redLeft}", style: TextStyle(color: CupertinoColors.destructiveRed))),
      ])),
    ])));

    return lst;
  }

  void _hitScore(Score s) {
    bool selected = this.selectionMap[s.id] == true;
    DatabaseHelper().setPlayerSelectionByScoreId(r.id, f.id, s.id, !selected).then((_) {
      this._refreshScoreList();
    });
  }

  Widget _buildBody() {
    var _festivalHeader = this._buildFestivalHeader();
    var _scoreList = this._buildUserScoreList();

    var _scrolledScoreList = ListView(
      children: _scoreList
    );

    return Column(
      children: _festivalHeader + [
        Expanded(child: _scrolledScoreList)
      ]
    );
  }

  void _addScore() {
    var route = CupertinoPageRoute(builder: (context) {
      return AddPlayerToFestival(
        restaurant: this.r,
        festival: this.f,
        maxTasks: this.tasksLeft,
        maxTrophies: this.yellowLeft);
    });

    Navigator.push(context, route).then((up) {
      // If up == null, then user cancelled adding a new player to this
      // festival.

      if(up != null && up.points != null) {
        // score = UserWithPoints
        var user = up.user;
        var points = up.points;
        var tasks = up.tasks;
        var score = Score(0, this.f.id, this.r.id, user.id, points, tasks);
        DatabaseHelper().addScore(score).then((int _scoreId) {
          this._refreshScoreList();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Text(i18n(this.context).playerAdd),
      onPressed: this.yellowLeft != null && this.yellowLeft > 0 ? _addScore : null);

    var _navBar = CupertinoNavigationBar(
      middle: Text(i18n(this.context).festEditPageTitle),
      previousPageTitle: i18n(this.context).mainFestivals,
      trailing: _addButton,
    );

    if(this.f == null) {
      return LoadingPage();
    } else {
      var body = this._buildBody();
      return CupertinoPageScaffold(
        navigationBar: _navBar,
        child: SafeArea(child: body),
      );
    }
  }
}
