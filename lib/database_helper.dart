import 'dart:async';
import 'dart:io' as io;

import 'package:async/async.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'package:cafe_app/township.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/score.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  static Database _db;

  static const String DB_FILENAME = "mycafe.db";
  static const String TABLE_TOWNSHIPS = "restaurants";
  static const String TABLE_USERS = "users";
  static const String TABLE_FESTIVALS = "festivals";
  static const String TABLE_SCORE = "score";
  static const String TABLE_SELECTION = "selection";
  static const String TABLE_METADATA = "configs";

  DatabaseHelper.internal();

  Future<Database> get db async {
    if(_db == null) {
      return await initIfNeeded() ? _db : null;
    } else {
      return _db;
    }
  }

  static Future<void> _addVersion2SelectionTable(Database db) async {
    print("DatabaseHelper: creating $TABLE_SELECTION");
    await db.execute("CREATE TABLE $TABLE_SELECTION (id integer primary key, rid int, fid int, sid int, flag int)");
  }

  static Future<void> _addVersion3TasksInfo(Database db) async {
    print("DatabaseHelper: adding information about tasks");
    await db.execute("ALTER TABLE $TABLE_SCORE add column tasks int default 0");
  }

  static Future<void> _addVersion3DistrInfo(Database db) async {
    await db.execute("ALTER TABLE $TABLE_FESTIVALS add column tasks int default 0");
    await db.execute("ALTER TABLE $TABLE_FESTIVALS add column method int default 1");
  }

  static Future<void> _addVersion4Metadata(Database db) async {
    await db.execute("CREATE TABLE $TABLE_METADATA (id integer primary key, confid int, conftext text)");
  }

  static Future<void> _upgradeFromVersion1(Database db) async {
    print("DatabaseHelper: upgradeFromVersion1");
    await _addVersion2SelectionTable(db);
  }

  static Future<void> _upgradeFromVersion2(Database db) async {
    print("DatabaseHelper: upgradeFromVersion2");
    await _addVersion3TasksInfo(db);
    await _addVersion3DistrInfo(db);
  }

  static Future<void> _upgradeFromVersion3(Database db) async {
    print("DatabaseHelper: upgradeFromVersion3");
    await _addVersion4Metadata(db);
  }

  // Increase this version on each change of the database schema.
  static const int DB_VERSION = 4;
  static Future<bool> initIfNeeded() async {
    var dbPath = await getDatabasesPath();
    var path = join(dbPath, DB_FILENAME);

    _db = await openDatabase(path,
      version: DB_VERSION,
      onCreate: (Database db, int version) async {
        print("DatabaseHelper: creating $TABLE_TOWNSHIPS");
        await db.execute("CREATE TABLE $TABLE_TOWNSHIPS (id integer primary key, name text)");
        print("DatabaseHelper: creating $TABLE_USERS");
        await db.execute("CREATE TABLE $TABLE_USERS (id integer primary key, rid integer, name text, disabled int)");
        print("DatabaseHelper: creating $TABLE_FESTIVALS");
        await db.execute("CREATE TABLE $TABLE_FESTIVALS (id integer primary key, rid integer, name text, date int, yellow int, red int, blue int)");
        print("DatabaseHelper: creating $TABLE_SCORE");
        await db.execute("CREATE TABLE $TABLE_SCORE (id integer primary key, rid int, fid int, uid int, yellow int)");
        await _addVersion2SelectionTable(db);
        await _addVersion3TasksInfo(db);
        await _addVersion3DistrInfo(db);
        await _addVersion4Metadata(db);
        print("DatabaseHelper: done");
      },
      onUpgrade: (Database db, int oldVer, int newVer) async {
        assert(newVer == DB_VERSION);
        switch(oldVer) {
          case 1:
            if(newVer >= 2)
              await _upgradeFromVersion1(db);

            if(newVer >= 3)
              await _upgradeFromVersion2(db);

            break;
          case 2:
            if(newVer >= 3)
              await _upgradeFromVersion2(db);

            break;
          case 3:
            if(newVer >= 4)
              await _upgradeFromVersion3(db);

            break;
          default:
            break;
            // shouldn't happen!
        }
      },
    );

    return true;
  }

  Future<int> saveTownship(Township r) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_TOWNSHIPS, r.toMapNoId());
    return res;
  }

  Future<int> saveUser(User u) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_USERS, u.toMapNoId());
    return res;
  }

  Future<int> countTownships() async {
    var dbClient = await db;
    var ret = await dbClient.rawQuery("SELECT count(*) as cnt from $TABLE_TOWNSHIPS");
    return ret.first['cnt'];
  }

  Future<List<Township>> loadTownships() async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_TOWNSHIPS,
      columns: ["id", "name"],
      orderBy: "name",
    );

    return ret.map((row) => Township.map(row)).toList();
  }

  Future<Township> getTownshipById(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_TOWNSHIPS,
      columns: ["id", "name"],
      where: "id=?",
      whereArgs: [id]);

    return Township.map(ret[0]);
  }

  Future<List<User>> loadActiveUsersForTownshipId(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_USERS,
      columns: ["id", "rid", "name", "disabled"],
      orderBy: "name",
      where: "disabled=0 and rid=?",
      whereArgs: [id],
    );

    return ret.map((row) => User.map(row)).toList();
  }

  Future<List<int>> loadUsersForFestivalId(int id) async {
    var dbc = await db;
    var ret = await dbc.rawQuery("select distinct(uid) as uid from $TABLE_SCORE where fid=?",
      [id]);

    var lst = List<int>();
    for(var row in ret) {
      lst.add(row['uid']);
    }

    return lst;
  }

  Future<Map<int, bool>> loadSelectionMapForFestivalId(int fid) async {
    var dbc = await db;
    var ret = await dbc.query(TABLE_SELECTION,
      columns: ["sid", "flag"],
      where: "fid=?",
      whereArgs: [fid]);

    var map = Map<int, bool>();
    for(var row in ret) {
      map[row["sid"]] = row["flag"] == 1;
    }

    return map;
  }

  Future<List<User>> loadAllUsersForTownshipId(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_USERS,
      columns: ["id", "rid", "name", "disabled"],
      orderBy: "name",
      where: "rid=?",
      whereArgs: [id],
    );

    return ret.map((row) => User.map(row)).toList();
  }

  Future<List<Festival>> loadFestivalsForTownshipId(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_FESTIVALS,
      columns: ["id", "rid", "name", "date", "yellow", "red", "blue"],
      orderBy: "date desc",
      where: "rid=?",
      whereArgs: [id]);

    return ret.map((row) => Festival.map(row)).toList();
  }

  Future<List<User>> loadActiveUsersForTownship(Township r) async =>
    this.loadActiveUsersForTownshipId(r.id);

  Future<int> addFestival(Festival f) async {
    var dbClient = await db;
    return dbClient.insert(TABLE_FESTIVALS, f.toMapNoId());
  }

  Future<void> rawRemoveUsersByTownshipId(int id) async {
    var dbc = await db;
    dbc.delete(TABLE_USERS, where: "rid=?", whereArgs: [id]);
    return 0;
  }

  Future<void> rawRemoveFestivalsByTownshipId(int id) async {
    var dbc = await db;
    dbc.delete(TABLE_FESTIVALS, where: "rid=?", whereArgs: [id]);
    return 0;
  }

  Future<void> rawRemoveFestivalById(int id) async {
    var dbc = await db;
    dbc.delete(TABLE_FESTIVALS, where: "id=?", whereArgs: [id]);
    return 0;
  }

  Future<void> rawRemoveTownshipById(int id) async {
    var dbc = await db;
    dbc.delete(TABLE_TOWNSHIPS, where: "id=?", whereArgs: [id]);
    return 0;
  }

  Future<void> rawRemoveScoresByFestivalId(int id) async {
    var dbc = await db;
    dbc.delete(TABLE_SCORE, where: "fid=?", whereArgs: [id]);
    return 0;
  }

  Future<void> rawRemoveScoresByTownshipId(int id) async {
    var dbc = await db;
    dbc.delete(TABLE_SCORE, where: "rid=?", whereArgs: [id]);
    return 0;
  }

  Future<void> rawRemoveSelectionsByFestivalId(int fid) async {
    var dbc = await db;
    dbc.delete(TABLE_SELECTION, where: "fid=?", whereArgs: [fid]);
  }

  Future<void> rawRemoveSelectionsByTownshipId(int rid) async {
    var dbc = await db;
    dbc.delete(TABLE_SELECTION, where: "rid=?", whereArgs: [rid]);
  }

  Future<void> removeFestivalById(int id) async {
    await this.rawRemoveFestivalById(id);
    await this.rawRemoveScoresByFestivalId(id);
    await this.rawRemoveSelectionsByFestivalId(id);
    return 0;
  }

  Future<void> removeTownshipById(int id) async {
    await this.rawRemoveFestivalsByTownshipId(id);
    await this.rawRemoveScoresByTownshipId(id);
    await this.rawRemoveUsersByTownshipId(id);
    await this.rawRemoveTownshipById(id);
    await this.rawRemoveSelectionsByTownshipId(id);
    return 0;
  }

  Future<void> removeFestival(Festival f) async =>
    this.removeFestivalById(f.id);

  Future<Festival> loadFestivalById(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_FESTIVALS,
      columns: ["id", "rid", "name", "date", "yellow", "red", "blue", "tasks", "method"],
      orderBy: "date desc",
      where: "id=?",
      whereArgs: [id]);

    return ret.map((row) => Festival.map(row)).toList()[0];
  }

  Future<Festival> getFestivalById(int id) async =>
    this.loadFestivalById(id);

  Future<void> removeUserById(int userId) async {
    var dbClient = await db;
    await dbClient.delete(TABLE_USERS,
      where: "id=?",
      whereArgs: [userId]);

    return 0;
  }

  Future<void> disableUserById(int userId) async {
    var dbClient = await db;
    await dbClient.update(TABLE_USERS, {"disabled": 1},
      where: "id=?",
      whereArgs: [userId]);

    return 0;
  }

  Future<void> removeScoreById(int scoreId) async {
    var dbClient = await db;
    await dbClient.delete(TABLE_SCORE,
      where: "id=?",
      whereArgs: [scoreId]);

    await dbClient.delete(TABLE_SELECTION,
      where: "sid=?",
      whereArgs: [scoreId]);

    return 0;
  }

  Future<bool> renameUserById(int uid, String newName) async {
    var dbc = await db;
    var changes = await dbc.update(TABLE_USERS, {"name": newName}, where: "id=?", whereArgs: [uid]);
    return changes > 0;
  }

  Future<User> getUserById(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_USERS,
      columns: ["id", "rid", "name"],
      where: "id=?",
      whereArgs: [id]);

    return User.map(ret[0]);
  }

  Future<int> countUsersForTownshipId(int rid) async {
    var dbClient = await db;
    var ret = await dbClient.rawQuery(
      "select count(*) as c from $TABLE_USERS where disabled=0 and rid=?",
      [rid]);

    return ret[0]["c"];
  }

  Future<void> clearDatabase() async {
    var dbPath = await getDatabasesPath();
    await io.File(join(dbPath, DB_FILENAME)).delete();
    await io.File(join(dbPath, "$DB_FILENAME-wal")).delete();
    await io.File(join(dbPath, "$DB_FILENAME-shm")).delete();
    return;
  }

  Future<int> addScore(Score s) async {
    var dbClient = await db;
    var scoreId = await dbClient.insert(TABLE_SCORE, s.toMapNoId());
    return scoreId;
  }

  Future<Score> getScoreById(int scoreId) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_SCORE,
      columns: ["id", "uid", "fid", "rid", "yellow", "tasks"],
      where: "id=?",
      whereArgs: [scoreId]);

    return Score.map(ret[0]);
  }

  Future<int> getScoreSumForUserId(int userId) async {
    var dbc = await db;
    var ret = await dbc.rawQuery(
      "select sum(yellow) as c from $TABLE_SCORE where uid=?",
      [userId]);

    return ret[0]["c"];
  }

  Future<List<Score>> getScoreListByFestivalId(int fid) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_SCORE,
      columns: ["id", "uid", "fid", "yellow", "tasks"],
      where: "fid=?",
      whereArgs: [fid]);

    var lst = ret.map((row) => Score.map(row)).toList();
    lst.sort((a, b) => b.yellow - a.yellow);
    return lst;
  }

  Future<int> countNumberOfFestivalsForUserId(int uid) async {
    var dbc = await db;
    var ret = await dbc.rawQuery(
      "select count(distinct(fid)) as c from $TABLE_SCORE where uid=?",
      [uid]);

    return ret[0]["c"];
  }

  Future<bool> isPlayerSelectedByScoreId(int sid) async {
    var dbc = await db;
    var ret = await dbc.rawQuery(
      "select flag from $TABLE_SELECTION where sid=?",
      [sid]);

    return ret[0]["flag"] == 1 ? true : false;
  }

  Future<void> setPlayerSelectionByScoreId(int rid, int fid, int sid, bool selected) async {
    var dbc = await db;
    var ret = await dbc.rawQuery("SELECT count(*) as cnt "
      "from $TABLE_SELECTION where sid=?", [sid]);

    if(ret[0]["cnt"] > 0) {
      if(selected) {
        await dbc.update(TABLE_SELECTION, { "flag": 1 },
          where: "sid=?",
          whereArgs: [sid]);
      } else {
        await dbc.delete(TABLE_SELECTION,
          where: "sid=?",
          whereArgs: [sid]);
      }
    } else {
      await dbc.insert(TABLE_SELECTION, {
        "sid": sid,
        "rid": rid,
        "fid": fid,
        "flag": selected ? 1 : 0,
      });
    }
  }

  Future<void> setFestivalMethod(int fid, int method) async {
    var dbc = await db;
    await dbc.update(TABLE_FESTIVALS, {"method": method},
      where: "id=?",
      whereArgs: [fid]);

    return;
  }

  Future<int> getFestivalMethod(int fid) async {
    var dbc = await db;
    var ret = await dbc.query(TABLE_FESTIVALS,
        columns: ["method"],
        where: "id=?",
        whereArgs: [fid]);

    return ret[0]["method"];
  }

  Future<void> deleteAllConfigs() async {
    var dbc = await db;
    await dbc.rawQuery("delete from $TABLE_METADATA");
  }

  Future<Result<String>> getConfigTextById(int configId) async {
    var dbc = await db;
    var ret = await dbc.query(TABLE_METADATA,
      columns: ["confid", "conftext"],
      where: "confid=?",
      whereArgs: [configId]);

    if(ret != null && ret.length > 0) {
      return Result.value(ret[0]["conftext"]);
    } else {
      return Result.error("No such config");
    }
  }

  Future<bool> doesConfigIdExists(int configId) async {
    var dbc = await this.db;
    var ret = await dbc.rawQuery("SELECT count(*) as cnt from $TABLE_METADATA where confid=$configId");
    return ret.first['cnt'] > 0;
  }

  Future<int> updateExistingConfigId(int configId, String configText) async {
    var dbc = await this.db;
    return dbc.update(TABLE_METADATA, {
      "conftext": configText
    }, where: "confid=?", whereArgs: [configId]);
  }

  Future<int> createNewConfigId(int configId, String configText) async {
    var dbc = await this.db;
    return dbc.insert(TABLE_METADATA, {
      "confid": configId,
      "conftext": configText,
    });
  }

  Future<bool> setConfigTextById(int configId, String configText) async {
    try {
      // lock db
      if(await this.doesConfigIdExists(configId)) {
        return 1 == await this.updateExistingConfigId(configId, configText);
      } else {
        return 1 == await this.createNewConfigId(configId, configText);
      }
    } finally {
      // unlock db
    }
  }
}
