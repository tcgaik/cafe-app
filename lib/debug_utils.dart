import 'package:meta/meta.dart';

typedef SimpleCallback = void Function();
typedef ParametrizedCallback<T> = T Function();

/// Run only in Debug mode
void run(SimpleCallback fn) {
  assert(() {
    fn();
    return true;
  }());
}

T invoke<T>({@required ParametrizedCallback<T> debugCode, @required ParametrizedCallback<T> releaseCode}) {
  bool release = true;
  assert(() {
    release = false;
    return true;
  }());

  return release ? releaseCode() : debugCode();
}