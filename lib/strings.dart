import 'dart:ui';
import 'package:cafe_app/i18n/messages_all.dart';
import 'package:flutter/widgets.dart';
import "package:intl/intl.dart";

StringLocalizations i18n(BuildContext ctx) => StringLocalizations.of(ctx);

class StringLocalizationsDelegate extends LocalizationsDelegate<StringLocalizations> {
  const StringLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    bool supportFlag = ['en', 'pl', 'es'].contains(locale.languageCode);
    return supportFlag;
  }

  @override
  Future<StringLocalizations> load(Locale locale) {
    print("loading StringLocalizations...");
    return StringLocalizations.load(locale);
  }

  @override
  bool shouldReload(StringLocalizationsDelegate old) => false;
}

class StringLocalizations {
  static Future<StringLocalizations> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return StringLocalizations();
    });
  }

  static StringLocalizations of(BuildContext context) {
    return Localizations.of<StringLocalizations>(context, StringLocalizations);
  }

  String get pasteButtonLabel =>
      Intl.message("Paste (strings.dart)",
          name: "pasteButtonLabel",
          desc: "Name of the Paste button used in edit boxes");

  String get townList =>
    Intl.message("My townships",
      name: "townList",
      desc: "Title of the township list page");

  String get townListUpper =>
    Intl.message("TOWNSHIP LIST",
      name: "townListUpper",
      desc: "Header of the actual township list, uppercase");

  String get townSwipeLeftToRemove =>
    Intl.message("(swipe left to remove)",
      name: "townSwipeLeftToRemove",
      desc: "Hint on the township list page");

  String get townNothingAddedYet =>
    Intl.message("You need to add a township!"
      "\n\nIn order to do this, please use the plus icon in the top right"
      " corner and then follow the instructions.",
      name: "townNothingAddedYet",
      desc: "Welcome message when there are no cities added yet");

  String get festNoPlayersAddedYet =>
    Intl.message("Add players to this festival!\n\nTap the 'Add Player' button\n"
      "in the top right corner.",
      name: "festNoPlayersAddedYet");

  String get globHello =>
    Intl.message("Hello!",
      name: "globHello");

  String get townAddTitle =>
    Intl.message("Add a township",
      name: "townAddTitle");

  String get townAddBody =>
    Intl.message("Enter the name of your new township.\n\nWhen finished, "
      "tap the 'Add' button in the top right corner.",
      name: "townAddBody");

  String get globAdd =>
    Intl.message("Add",
      name: "globAdd");

  String get globRemove =>
    Intl.message("Remove",
      name: "globRemove");

  String get globDoNothing =>
    Intl.message("Do nothing",
      name: "globDoNothing");

  String get mainSelectOption =>
    Intl.message("Please select desired option from the menu below.",
      name: "mainSelectOption");

  String get mainPlayers =>
    Intl.message("Players",
      name: "mainPlayers");

  String get mainManagement =>
    Intl.message("Management",
      name: "mainManagement");

  String get playerAdd =>
    Intl.message("New player",
      name: "playerAdd");

  String get mainAddNewPlayer =>
    Intl.message("Add a new player",
      name: "mainAddNewPlayer");

  String get festAddPlayer =>
    Intl.message("Add player to festival",
      name: "festAddPlayer");

  String get playerAddNew =>
    Intl.message("Add a new player",
      name: "playerAddNew");

  String get playerRenameBody =>
    Intl.message("What is the new name of this player?",
      name: "playerRenameBody");

  String get playerRenameTitle =>
    Intl.message("Rename player",
      name: "playerRenameTitle");

  String get playerName =>
    Intl.message("Player name",
      name: "playerName");

  String get playerRename =>
    Intl.message("Rename",
      name: "playerRename");

  String get mainManagePlayers =>
    Intl.message("Manage players",
      name: "mainManagePlayers");

  String get mainAbout =>
    Intl.message("About this app",
      name: "mainAbout");

  String get mainFestivals =>
    Intl.message("Festivals",
      name: "mainFestivals");

  String get mainNewFestival =>
    Intl.message("New festival",
      name: "mainNewFestival");

  String get mainFestivalHistory =>
    Intl.message("Festival history",
      name: "mainFestivalHistory");

  String get playerNewNameBody =>
    Intl.message("What is the name of this new player?",
      name: "playerNewNameBody");

  String get festDistributionGoal =>
    Intl.message("Distribution goal:",
      name: "festDistributionGoal");

  String get festPlayerList =>
    Intl.message("Player list.\n\nTap to toggle.",
      name: "festPlayerList");

  String get festPlayerRemoved =>
    Intl.message("(inactive)",
      name: "festPlayerRemoved");

  String get festSummary =>
    Intl.message("What's left?",
      name: "festSummary");

  String get globLoading =>
    Intl.message("Loading...",
      name: "globLoading");

  String get festEditPageTitle =>
    Intl.message("Edit festival",
      name: "festEditPageTitle");

  String get festBackButtonTitle =>
    Intl.message("Festival",
      name: "festBackButtonTitle");

  String get globOk =>
    Intl.message("OK",
      name: "globOk");

  String get globCancel =>
    Intl.message("Cancel",
      name: "globCancel");

  String get globWarning =>
    Intl.message("Warning!",
      name: "globWarning");

  String get globCreate =>
    Intl.message("Create",
      name: "globCreate");

  String get festNoPlayersAvailable =>
    Intl.message("(none)",
      name: "festNoPlayersAvailable");

  String get aboutPageTitle =>
    Intl.message("About",
      name: "aboutPageTitle");

  String get globPleaseWait =>
    Intl.message("Please wait...",
      name: "globPleaseWait");

  String get aboutAppName =>
    Intl.message("MyCafe Calculator",
      name: "aboutAppName");

  String get aboutWrittenBy =>
    Intl.message("Written by",
      name: "aboutWrittenBy");

  String get aboutDedicatedTo =>
    Intl.message("Dedicated to",
      name: "aboutDedicatedTo");

  String get aboutTranslationBy =>
    Intl.message("Translated by",
      name: "aboutTranslationBy");

  String get aboutTranslationAuthor =>
    Intl.message("(missing name)",
      name: "aboutTranslationAuthor");

  String get aboutOpenBlog =>
    Intl.message("Open blog",
      name: "aboutOpenBlog");

  String get aboutOpenSource =>
      Intl.message("Open source code",
          name: "aboutOpenSource");

  String get aboutThirdpartyStuff =>
    Intl.message("Thirdparty stuff:",
      name: "aboutThirdpartyStuff");

  String get aboutWrittenInFlutter =>
    Intl.message("Written in Flutter!",
      name: "aboutWrittenInFlutter");

  String get playerInstructionText =>
    Intl.message("Swipe left to remove the player. Long tap to rename the "
      "player.",
      name: "playerInstructionText");

  String get festNeedMorePlayers =>
    Intl.message("You need to add more players to this township!",
      name: "festNeedMorePlayers");

  String get festCheckYourPoints =>
    Intl.message("Make sure you've distributed your points correctly.",
      name: "festCheckYourPoints");

  String get festSelectPlayer =>
    Intl.message("Select player by tapping his/her name.",
      name: "festSelectPlayer");

  String get mainRemoveTownshipTitle =>
    Intl.message("Remove this township",
      name: "mainRemoveTownshipTitle");

  String get mainRemoveTownshipBody =>
    Intl.message("Do you want to permanently remove this township, "
      "all its users and festivals?",
        name: "mainRemoveTownshipBody");

  String get globTrophyPoints =>
    Intl.message("Trophy points",
      name: "globTrophyPoints");

  String get globQuestPoints =>
    Intl.message("Quest points",
      name: "globQuestPoints");

  String get festSetTrophyPointsTitle =>
    Intl.message("Set trophy points",
      name: "festSetTrophyPointsTitle");

  String get festSetQuestPointsTitle =>
    Intl.message("Set quest points",
      name: "festSetQuestPointsTitle");

  String setTrophyPointsBody(int minValue, int maxValue) =>
    Intl.message("How many trophies did this user get?\n\nEnter a value "
        "between $minValue and $maxValue.",
      args: [minValue, maxValue],
      name: "setTrophyPointsBody");

  String setQuestPointsBody(int minValue, int maxValue) =>
    Intl.message("How many quests has been completed by this user?\n\nEnter a "
      "value between $minValue and $maxValue.",
    args: [minValue, maxValue],
    name: "setQuestPointsBody");

  String get festNoFestivalsAddedYet =>
    Intl.message("No festivals added yet.",
      name: "festNoFestivalsAddedYet");

  String get festChooseProperties =>
    Intl.message("Choose properties:",
      name: "festChooseProperties");

  String get festDate =>
    Intl.message("Date",
      name: "festDate");

  String get festDiamonds =>
    Intl.message("Diamonds",
      name: "festDiamonds");

  String get festRubies =>
    Intl.message("Rubies",
      name: "festRubies");

  String get festTrophyCount =>
    Intl.message("Trophy count",
      name: "festTrophyCount");

  String get festQuestCount =>
    Intl.message("Quest count",
      name: "festQuestCount");

  String get festOnlyTrophies =>
    Intl.message("Only trophies",
    name: "festOnlyTrophies");

  String get festUseQuests =>
    Intl.message("Add quest count",
    name: "festUseQuests");

  String get festQuests =>
    Intl.message("Quests",
    name: "festQuests");

  String get festChoose =>
    Intl.message("Choose:",
    name: "festChoose");

  String get festDiamondCount =>
    Intl.message("Diamond count",
      name: "festDiamondCount");

  String get festRubyCount =>
    Intl.message("Ruby count",
      name: "festRubyCount");

  String get festTrophiesReceivedTitle =>
    Intl.message("How many trophies have you received?",
      name: "festTrophiesReceivedTitle");

  String get festQuestReceivedTitle =>
    Intl.message("How many quests were completed?",
      name: "festQuestReceivedTitle");

  String get festRubyReceivedTitle =>
    Intl.message("How many rubies have you received?",
      name: "festRubyReceivedTitle");

  String get festDiamondReceivedTitle =>
    Intl.message("How many diamonds have you received?",
      name: "festDiamondReceivedTitle");

  String get festControlInstructions =>
    Intl.message("Slide left to remove. Tap to edit.",
      name: "festControlInstructions");

  static String get globBack =>
    Intl.message("Back",
      name: "globBack");

  String globTrophySuffix(value) =>
    Intl.plural(value,
      args: [value],
      zero: "trophies",
      one: "trophy",
      two: "trophies",
      many: "trophies",
      other: "trophies",
      name: "globTrophySuffix");

  String globQuestSuffix(int value) =>
    Intl.plural(value,
      args: [value],
      zero: "quests",
      one: "quest",
      two: "quests",
      many: "quests",
      other: "quests",
      name: "globQuestSuffix");

  String globPointSuffix(int value) =>
    Intl.plural(value,
      args: [value],
      zero: "points",
      one: "point",
      two: "points",
      many: "points",
      other: "points",
      name: "globPointSuffix");

  String globDiamondSuffix(int value) =>
    Intl.plural(value,
      args: [value],
      zero: "diamonds",
      one: "diamond",
      two: "diamonds",
      many: "diamonds",
      other: "diamonds",
      name: "globDiamondSuffix");

  String globRubySuffix(int value) =>
    Intl.plural(value,
      args: [value],
      zero: "rubies",
      one: "ruby",
      two: "rubies",
      many: "rubies",
      other: "rubies",
      name: "globRubySuffix");

  String get globThankYou => Intl.message("Thank you!", name: "globThankYou");
  String get globSorry => Intl.message("Sorry!", name: "globSorry");
  String get globVoteDialogQuestion => Intl.message("Do you think this app is useful?",
    name: "globVoteDialogQuestion");

  String get globVoteYesAnswer => Intl.message("Would you like to rate the "
      "app in Play Store, so that other people will also know about it?",
    name: "globVoteYesAnswer");

  String get globVoteNoAnswer => Intl.message("I'm sorry the app doesn't "
      "perform as you'd like. If you have any suggestions, you can contact "
      "the author directly! Do you want to compose an e-mail now?",
    name: "globVoteNoAnswer");

  String get emailFeedbackSubject => Intl.message("MyCafe Calculator feedback",
    name: "emailFeedbackSubject");

  String get emailNegativeFeedbackBody => Intl.message("Hi. I don't like your "
      "MyCafe Calculator. Here's why:",
    name: "emailNegativeFeedbackBody");

  String get voteYes => Intl.message("Yes! 😀", name: "voteYes");
  String get voteNo => Intl.message("No 😟", name: "voteNo");
  String get voteEMail => Intl.message("E-Mail", name: "voteEMail");
  String get voteRateIt => Intl.message("Rate it", name: "voteRateIt");
  String get globQuestion => Intl.message("Question", name: "globQuestion");
}
