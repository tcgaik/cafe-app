import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

typedef OKCallbackFunc = void Function(String newName);

class UserDialogs {
  static Future<bool> yesNoDialog(BuildContext ctx,
      { @required String title,
        @required String message,
        @required VoidCallback yesHandler,
        @required VoidCallback noHandler,
        String yesLabel = "Yes",
        String noLabel = "No",
      })
  {
    var _yesButton = CupertinoButton(
        child: Text(yesLabel),
        onPressed: () {
          Navigator.of(ctx).pop();
          yesHandler();
        }
    );

    var _noButton = CupertinoButton(
        child: Text(noLabel),
        onPressed: () {
          Navigator.of(ctx).pop();
          noHandler();
        }
    );

    var _dialog = CupertinoAlertDialog(
        title: Text(title),
        content: Text("\n" + message),
        actions: [_yesButton, _noButton]);

    return showCupertinoDialog<bool>(
      context: ctx,
      builder: (_) => _dialog,
    );
  }

  static Future<bool> alertYesNoDialog(BuildContext ctx,
      { @required String title,
        @required String message,
        @required VoidCallback yesHandler,
        @required VoidCallback noHandler,
        String yesLabel = "Yes",
        String noLabel = "No",
      })
  {
    var _redStyle = TextStyle(color: Colors.red);
    var _yesButton = CupertinoButton(
      child: Text(yesLabel, style: _redStyle),
      onPressed: () {
        Navigator.of(ctx).pop();
        yesHandler();
      }
    );

    var _noButton = CupertinoButton(
      child: Text(noLabel),
      onPressed: () {
        Navigator.of(ctx).pop();
        noHandler();
      }
    );

    var _dialog = CupertinoAlertDialog(
      title: Text(title),
      content: Text("\n" + message),
      actions: [_yesButton, _noButton]);

    return showCupertinoDialog<bool>(
      context: ctx,
      builder: (_) => _dialog,
    );
  }

  static Future<String> showPlayerRenameDialog(BuildContext ctx, {@required String title, @required GlobalKey<FormState> key, @required OKCallbackFunc onOK, String initialText}) {
    var ret = showDialog<String>(
      context: ctx,
      barrierDismissible: false,
      builder: (ctx) {
        return AlertDialog(
          title: Text(title),
          content: Form(
            key: key,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  autofocus: true,
                  initialValue: initialText,
                  onSaved: onOK,
                  validator: (data) {
                    if(data.isEmpty) return "Enter something!";
                    if(data.trim().isEmpty)
                      return "Enter something sensible...";
                  }
                )
              ]
            )
          ),
          actions: [
            FlatButton(
              child: Text("OK"),
              onPressed: () {
                if(key.currentState.validate()) {
                  key.currentState.save();
                  Navigator.of(ctx).pop();
                }
              },
            ),
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
          ],
        );
      }
    );
    return ret;
  }
}
