import 'package:flutter/cupertino.dart';
import 'package:cafe_app/strings.dart';

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  StringLocalizations _;

  Widget _buildPage() {
    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [CupertinoActivityIndicator()],
    );

    var _paddedCaption = Padding(
      padding: EdgeInsets.all(16),
      child: Text(_.globPleaseWait));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    return _padded;
  }

  @override
  Widget build(BuildContext context) {
    this._ = StringLocalizations.of(this.context);

    var _page = this._buildPage();
    var _navBar = CupertinoNavigationBar(
      middle: Text(_.globLoading),
    );

    return CupertinoPageScaffold(
      navigationBar: _navBar,
      child: SafeArea(child: _page)
    );
  }
}
