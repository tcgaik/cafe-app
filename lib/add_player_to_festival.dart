import 'package:flutter/cupertino.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/property_list_view.dart';

class UserComboItem implements ComboInner {
  final User u;

  UserComboItem.from(this.u);
  UserComboItem({this.u});

  @override
  String getValueString() {
    return u.name;
  }
}

class UserWithPoints {
  User _u;
  int _points;
  int _tasks;

  UserWithPoints(this._u, this._points, this._tasks);

  get user => _u;
  get points => _points;
  get tasks => _tasks;
}

class AddPlayerToFestival extends StatefulWidget {
  AddPlayerToFestival({Key key,
    @required this.festival,
    @required this.restaurant,
    @required this.maxTasks,
    @required this.maxTrophies}): super(key: key);

  final Township restaurant;
  final Festival festival;
  final int maxTrophies;
  final int maxTasks;

  @override
  _AddPlayerToFestivalState createState() => _AddPlayerToFestivalState();
}

class _AddPlayerToFestivalState extends State<AddPlayerToFestival> {
  var _userList = new List<UserComboItem>();
  int _currentUser;
  int _currentTrophies;
  int _currentTasks;
  int _minValue = 1;
  int _maxValue = 0;
  int _minTaskValue = 1;
  int _maxTaskValue = 0;

  @override
  void initState() {
    super.initState();
    this._currentUser = 0;
    this._currentTrophies = 0;
    this._currentTasks = 0;
    this._maxValue = this.widget.maxTrophies;
    this._maxTaskValue = this.widget.maxTasks;
    _refreshPlayerList();
  }

  Future<List<User>> _doRefreshPlayerList() async {
    var userList = await DatabaseHelper().loadActiveUsersForTownship(widget.restaurant);
    var fUserList = await DatabaseHelper().loadUsersForFestivalId(widget.festival.id);

    var nUserList = List<User>();
    for(User u in userList) {
      if(!fUserList.contains(u.id)) {
        nUserList.add(u);
      }
    }

    return nUserList;
  }

  void _refreshPlayerList() {
    this._doRefreshPlayerList().then((ulist) {
      setState(() {
        this._userList = ulist.map((u) => UserComboItem.from(u)).toList();
        if(this._userList.length == 1) {
          this._minValue = this._maxValue;
          this._minTaskValue = this._maxTaskValue;
          this._currentTrophies = this._maxValue;
          this._currentTasks = this._maxTaskValue;
        }
      });
    });
  }

  Widget _buildBody(BuildContext ctx) {
    var _staticItems = <AbstractPropertyWidget>[];
    if(this._userList.length == 0) {
      _staticItems.add(LinePropertyWidget.from(i18n(this.context).festNeedMorePlayers));
      _staticItems.add(LinePropertyWidget.from(i18n(this.context).festCheckYourPoints));
    }

    return Container(
      child: PropertyListView(
        items: _staticItems + <AbstractPropertyWidget>[
          LinePropertyWidget.from(i18n(this.context).festSelectPlayer),

          ComboPropertyWidget.from(this.context, i18n(this.context).playerName, this._userList,
            currentObject: this._currentUser,
            stateUpdate: (idx) => setState(() { this._currentUser = idx; })),

          NumericPropertyWidget.from(i18n(this.context).globTrophyPoints, this._currentTrophies,
            editTitle: i18n(this.context).festSetTrophyPointsTitle,
            editBody: i18n(this.context).setTrophyPointsBody(this._minValue, this._maxValue),
            customValidator: (n) => n >= this._minValue && n <= this._maxValue,
            setCallback: (n) => setState(() { this._currentTrophies = n; })),

          this.widget.festival.tasks > 0 ?
            NumericPropertyWidget.from(i18n(this.context).globQuestPoints, this._currentTasks,
              editTitle: i18n(this.context).festSetQuestPointsTitle,
              editBody: i18n(this.context).setQuestPointsBody(this._minTaskValue, this._maxTaskValue),
              customValidator: (n) => n >= this._minTaskValue && n <= this._maxTaskValue,
              setCallback: (n) => setState(() { this._currentTasks = n; })) :
            null,
        ].where((o) => o != null).toList(),
      ),
    );
  }

  bool _validateForm() {
    // There are no users to choose from.
    if(this._userList.length == 0)
      return false;

    // Player is not selected.
    if(this._currentUser == -1)
      return false;

    // User didn't fill out trophy count for this player yet.
    if(this._currentTrophies == 0)
      return false;

    // We're using task counter, but user didn't enter any task count for this
    // user yet.
    if(this._maxTaskValue > 0 && this._currentTasks == 0)
      return false;

    return true;
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Text(i18n(this.context).globOk),
      onPressed: this._validateForm() ? () {
          var userIdx = this._currentUser;
          var user = this._userList[userIdx].u;
          var points = this._currentTrophies;
          var tasks = this._currentTasks;
          Navigator.of(context).pop(UserWithPoints(user, points, tasks));
        } : null);

    var _navBar = CupertinoNavigationBar(
      middle: Text(i18n(this.context).festAddPlayer),
      previousPageTitle: i18n(this.context).festBackButtonTitle,
      trailing: _addButton,
    );

    return CupertinoPageScaffold(
      navigationBar: _navBar,
      child: SafeArea(child: _buildBody(context)),
    );
  }
}

