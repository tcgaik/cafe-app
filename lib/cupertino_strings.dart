import 'dart:ui';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import "package:intl/intl.dart";
import 'package:flutter/services.dart' show rootBundle;

class PluralItem {
  String zero;
  String one;
  String two;
  String many;
  String other;

  PluralItem(this.zero, this.one, this.two, this.many, this.other);

  PluralItem.simple(String one, String many) {
    PluralItem(many, one, many, many, many);
  }

  PluralItem.empty() {
    PluralItem.simple(null, null);
  }

  String getString(int howMany) {
    if(other == null) {
      return "#?#";
    }

    return Intl.pluralLogic(howMany, zero: zero, one: one, two: two,
        many: many, other: other);
  }

  @override
  String toString() {
    return "PluralItem[zero=$zero,one=$one,two=$two,many=$many,other=$other]";
  }
}

PluralItem _emptyPlural = PluralItem.empty();

class LocalCupertinoLocalizationsDelegate extends LocalizationsDelegate<CupertinoLocalizations> {
  @override bool isSupported(Locale locale) => ['es', 'pl', 'en'].contains(locale.languageCode);
  @override Future<CupertinoLocalizations> load(Locale locale) => LocalCupertinoLocalizations.load(locale);
  @override bool shouldReload(LocalizationsDelegate<CupertinoLocalizations> old) => false;
  @override String toString() => 'LocalCupertinoLocalizations.delegate(all locales)';
}

Future<String> _loadAsset(String filename) async {
  return rootBundle.loadString(filename);
}

class LocalCupertinoLocalizations implements CupertinoLocalizations {
  String localeName;

  var jsonObject = Map<String, dynamic>();
  var plurals = Map<String, PluralItem>();

  static Future<CupertinoLocalizations> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return _createFuture(localeName);
  }

  static Future<CupertinoLocalizations> _createFuture(String localeName) async {
    var filename = "assets/i18n/cupertino_$localeName.arb";
    print("Loading translation ARB object: $filename");
    var contents = await _loadAsset(filename);

    try {
      return LocalCupertinoLocalizations(localeName, jsonDecode(contents));
    } on FormatException catch(_) {
      return null;
    }
  }

  LocalCupertinoLocalizations(this.localeName, this.jsonObject) {
    this._initWeekDays();
    this._initMonths();
    this._initShortMonths();
    this._initPlurals();
  }

  String _lookupString(String msg) {
    var str = this.jsonObject[msg] ?? "#{$msg}";
    return str;
  }

  PluralItem _lookupPlural(String msg) => this.plurals[msg] ?? _emptyPlural;

  @override
  String get alertDialogLabel => this._lookupString("alertDialogLabel");

  @override
  String get anteMeridiemAbbreviation => this._lookupString("anteMeridiemAbbreviation");

  @override
  String get copyButtonLabel => this._lookupString("copyButtonLabel");

  @override
  String get cutButtonLabel => this._lookupString("cutButtonLabel");

  DatePickerDateOrder _datePickerDateOrderCache;

  @override
  DatePickerDateOrder get datePickerDateOrder {
    if(this._datePickerDateOrderCache != null)
      return this._datePickerDateOrderCache;

    var localeOrder = this._lookupString("datePickerDateOrder");
    switch(localeOrder) {
      case "dmy":
        this._datePickerDateOrderCache = DatePickerDateOrder.dmy;
        break;
      case "mdy":
        this._datePickerDateOrderCache = DatePickerDateOrder.mdy;
        break;
      case "ymd":
        this._datePickerDateOrderCache = DatePickerDateOrder.ymd;
        break;
      case "ydm":
        this._datePickerDateOrderCache = DatePickerDateOrder.ydm;
        break;
      default:
        this._datePickerDateOrderCache = DatePickerDateOrder.dmy;
        break;
    }

    return this._datePickerDateOrderCache;
  }

  @override
  DatePickerDateTimeOrder get datePickerDateTimeOrder => DatePickerDateTimeOrder.time_dayPeriod_date;

  @override
  String datePickerDayOfMonth(int dayIndex) => dayIndex.toString();

  @override
  String datePickerHour(int hour) => hour.toString();

  List<String> _makeTable(String input) =>
      input.split(",").map((String s) => s.trim()).toList();

  static List<String> _shortWeekdays;
  void _initWeekDays() {
    _shortWeekdays = this._makeTable(this._lookupString("globWeekDays"));
  }

  static List<String> _shortMonths;
  void _initShortMonths() {
    _shortMonths = this._makeTable(this._lookupString("globMonths"));
  }

  static List<String> _months;
  void _initMonths() {
    _months = this._makeTable(this._lookupString("globMonthsLow"));
  }

  void _initPlurals() {
    this.jsonObject.keys.forEach((String key) {
      String val = this.jsonObject[key].toString();
      var token = "{value,plural, ";

      String zero, one, two, many, other;

      if(val.startsWith(token)) {
        var inner = val.substring(token.length, val.length - 1).trim();
        var reg = RegExp(r"([0-9a-zA-z=]+)\{([^}]+)\}");

        reg.allMatches(inner).forEach((Match m) {
          var spec = m.group(1);
          var text = m.group(2);

          switch(spec) {
            case "=0":
              zero = text;
              break;
            case "=1":
              one = text;
              break;
            case "=2":
              two = text;
              break;
            case "many":
              many = text;
              break;
            case "other":
              other = text;

              this.plurals[key] = PluralItem(zero, one, two, many, other);
              // print("Adding plural: $key = ${this.plurals[key]}");
              break;
          }
        });
      }
    });
  }

  @override
  String datePickerMediumDate(DateTime date) {
    return '${_shortWeekdays[date.weekday - DateTime.monday]} '
        '${_shortMonths[date.month - DateTime.january]} '
        '${date.day.toString().padRight(2)}';
  }

  @override
  String datePickerMinute(int minute) => minute.toString().padLeft(2, '0');

  @override
  String datePickerMinuteSemanticsLabel(int minute) {
    return "";
  }

  @override
  String datePickerMonth(int monthIndex) => _months[monthIndex - 1];

  @override
  String datePickerYear(int yearIndex) => yearIndex.toString();

  @override
  String get pasteButtonLabel => this._lookupString("pasteButtonLabel");

  @override
  String get postMeridiemAbbreviation => this._lookupString("postMeridiemAbbreviation");

  @override
  String get selectAllButtonLabel => this._lookupString("selectAllButtonLabel");

  @override
  String timerPickerHour(int hour) => hour.toString();

  @override
  String timerPickerMinute(int minute) => minute.toString();

  @override
  String timerPickerSecond(int second) => second.toString();

  @override
  String timerPickerSecondLabel(int second) =>
      this._lookupPlural("globSecond").getString(second);

  @override
  String timerPickerHourLabel(int hour) =>
      this._lookupPlural("globHour").getString(hour);

  @override
  String timerPickerMinuteLabel(int minute) =>
      this._lookupPlural("globMinute").getString(minute);

  @override
  String datePickerHourSemanticsLabel(int hour) {
    var template = this._lookupString("globHour");
    return template.replaceAll("{hour}", hour.toString());
  }
}
