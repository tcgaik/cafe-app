import 'package:cafe_app/configs/config_loader.dart';
import 'package:cafe_app/configs/vote_dialog_config.dart';
import 'package:cafe_app/configs/first_run_config.dart';

class GlobalConfig {
  VoteDialogConfig _voteDialogConfig;
  FirstRunConfig _firstRunConfig;

  int get runningInDays =>
      DateTime.now().difference(this._firstRunConfig.firstRunDate).inDays;

  DateTime get lastVoteDisplay =>
    this._voteDialogConfig.lastDisplayDate;

  int get lastVersionCode =>
    this._voteDialogConfig.lastVersionCode;

  // How many days ago the Vote dialog was shown?
  int get lastVoteDisplayInDays =>
      DateTime.now().difference(this._voteDialogConfig.lastDisplayDate).inDays;

  // Don't ever show the Vote dialog.
  bool get neverShowVoteDialog => this._voteDialogConfig.neverDisplay;

  // How many days must pass after first app run to show the Vote dialog?
  int get voteDialogMinimumDaysFromFirstRun => 14;

  // How many days from last display of Vote dialog must pass to display the
  // dialog again?
  int get voteDialogMinimumDaysFromLastDisplay => 45;

  factory GlobalConfig() {
    if(_globalConfigInstance == null)
      _globalConfigInstance = GlobalConfig._createNew();

    return _globalConfigInstance;
  }

  GlobalConfig._createNew() {
    this.forceLoad();
  }

  Future<void> forceLoad() async {
    await this._loadFirstRunConfig();
    await this._loadVoteDialogConfig();
  }

  Future<void> _loadFirstRunConfig() async {
    var firstRunConfig = await ConfigLoader.getFirstRunConfig();

    if(firstRunConfig.isValue) {
      this._firstRunConfig = firstRunConfig.asValue.value;
    } else {
      this._firstRunConfig = FirstRunConfig(DateTime.now());
      this._persistFirstRunConfig();
    }
  }

  Future<void> _loadVoteDialogConfig() async {
    var voteDialogConfig = await ConfigLoader.getVoteDialogConfig();
    if(voteDialogConfig.isValue) {
      this._voteDialogConfig = voteDialogConfig.asValue.value;
    } else {
      this._voteDialogConfig = VoteDialogConfig(DateTime.now(), false);
      this._persistVoteDialogConfig();
    }
  }

  Future<bool> _persistFirstRunConfig() async =>
      ConfigLoader.saveFirstRunConfig(this._firstRunConfig);

  Future<bool> _persistVoteDialogConfig() async =>
      ConfigLoader.saveVoteDialogConfig(this._voteDialogConfig);

  set neverShowVoteDialog(bool flag) {
    this._voteDialogConfig.neverDisplay = flag;
    this._persistVoteDialogConfig();
  }

  set lastVoteDisplay(DateTime when) {
    this._voteDialogConfig.lastDisplayDate = when;
    this._persistVoteDialogConfig();
  }

  set lastVersionCode(int code) {
    this._voteDialogConfig.lastVersionCode = code;
    this._persistVoteDialogConfig();
  }
}

GlobalConfig _globalConfigInstance;
