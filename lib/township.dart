class Township {
  String _name;
  int _id;

  Township(this._id, this._name);

  Township.map(dynamic obj) {
    this._id = obj['id'];
    this._name = obj['name'];
  }

  int get id => _id;
  String get name => _name;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }

  Map<String, dynamic> toMapNoId() {
    var map = toMap();
    map.remove("id");
    return map;
  }
}