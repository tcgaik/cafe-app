import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/township_home_page.dart';
import 'package:cafe_app/dismiss.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/list_view_item.dart';

class TownshipList extends StatefulWidget {
  final VoidCallback onBack;
  final List<Township> items;

  TownshipList({Key key, this.items, this.onBack}): super(key: key);

  @override
  _TownshipListState createState() => new _TownshipListState();
}

class _TownshipListState extends State<TownshipList> {
  @override
  void initState() {
    super.initState();
  }

  void _listTap(BuildContext context, Township r) {
    Navigator.push(context, CupertinoPageRoute(
      builder: (context) => TownshipHomePage(restaurant: r),
    )).then((_) { this.widget.onBack(); });
  }

  Widget _dismiss(Township r, Widget w) {
    return Dismissible(
      key: Key("${r.id}"),
      direction: DismissDirection.endToStart,
      background: dismissRemoveContainer(this.context),
      child: w,
      onDismissed: (dir) {
        DatabaseHelper().removeTownshipById(r.id).then((ret) => setState(() {
          widget.items.remove(r);
        }));
      }
    );
  }

  Widget _buildListView(BuildContext context, List<Township> items) {
    var _image = Image.asset("assets/restaurant_list_header.png");
    var _imageContainer = Container(
      child: _image);

    var _restaurantListCaption = Text(i18n(this.context).townListUpper,
      style: TextStyle(
        fontSize: 10,
        fontWeight: FontWeight.bold,
        color: Colors.grey,
        letterSpacing: 4));

    var _restaurantListCaptionRow = Row(
      children: [
        Expanded(child: _restaurantListCaption),
        Padding(
          padding: EdgeInsets.only(right: 16),
          child:
            Text(i18n(this.context).townSwipeLeftToRemove,
              style: TextStyle(
                fontSize: 10,
                color: Colors.grey)))
      ]);

    var _restaurantListCaptionContainer = Padding(
      padding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
      child: _restaurantListCaptionRow);

    var _listViewChildren = <Widget> [
    ] + items.map((Township r) {
      var _tile = ListViewItem(
        title: r.name,
        onTap: () => _listTap(context, r));

      var _tileContainer = Padding(
        padding: EdgeInsets.symmetric(horizontal: 8),
        child: _tile);

      return _dismiss(r, _tileContainer);
    }).toList();

    var _listView = ListView(
      padding: EdgeInsets.all(0),
      children: _listViewChildren,
    );

    var _column = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        this._isLandscape() ? null : _imageContainer,
        _restaurantListCaptionContainer,
        Expanded(child: _listView)
      ].where((i) => i != null).toList()
    );

    return _column;
  }

  bool _isLandscape() =>
    MediaQuery.of(this.context).orientation == Orientation.landscape;

  @override Widget build(BuildContext context) {
    return SafeArea(child: _buildListView(context, widget.items));
  }
}
