import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter/cupertino.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/fullscreen_message_page.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/township_list.dart';
import 'package:cafe_app/loading_page.dart';
import 'package:cafe_app/debug_utils.dart' as DebugUtils;
import 'package:cafe_app/global_config.dart';

class ShowNotificationIcon {
  void show(BuildContext context) async {
    OverlayState overlayState = Overlay.of(context);
    OverlayEntry overlayEntry = new OverlayEntry(builder: _build);
    overlayState.insert(overlayEntry);
    await new Future.delayed(const Duration(seconds: 2));
    overlayEntry.remove();
  }

  Widget _build(BuildContext context){
    return new Positioned(
      top: 50.0,
      left: 50.0,
      child: Text("abc")
    );
  }
}


class MyTownshipsPage extends StatefulWidget {
  MyTownshipsPage({Key key, this.title}): super(key: key);

  final String title;

  @override
  _MyTownshipsPageState createState() => new _MyTownshipsPageState();
}

class _MyTownshipsPageState extends State<MyTownshipsPage> {
  List<Township> _restaurantList;
  bool _loading = true;
  bool _debugMode = false;
  bool _firstRunCompleted = false;

  @override void initState() {
    super.initState();

    DebugUtils.run(() {
      setState(() => this._debugMode = true);
    });

    _firstRun().then((_) => setState(() {
      this._firstRunCompleted = true;
    }));

    _refreshRestaurantList();
  }

  Future<Null> _firstRun() async {
    // Ensure GlobalConfig object is up and running when we'll try to use it later
    GlobalConfig();
  }

  void _locDebugDump() {
    var loc = CupertinoLocalizations.of(this.context);
    print("CupertinoLocalizations=$loc");
    print("paste button label = '${loc.pasteButtonLabel}'");
  }

  void _refreshRestaurantList() {
    setState(() { _loading = true; });

    DatabaseHelper().loadTownships().then((List<Township> restaurantList) {
      setState(() {
        _restaurantList = restaurantList;
        _loading = false;
      });
    });
  }

  void _showAddRestaurantPage(BuildContext context) {
    Navigator.pushNamed(context, "/addrest").then((result) {
      // `result` should be 'int 1'
      if(result != null) {
        _refreshRestaurantList();
      } else {
        // Nothing.
      }
    });
  }

  void _showDebugPage(BuildContext context) {
    DebugUtils.run(() {
      _locDebugDump();
    });

    Navigator.pushNamed(context, "/debug").then((_) {
      _refreshRestaurantList();
    });
  }

  Widget _buildLoadedBody(BuildContext context) {
    int cnt = _restaurantList.length;

    if(cnt == 0) {
      return FullscreenMessagePage(message: i18n(this.context).townNothingAddedYet);
    } else {
      return TownshipList(items: _restaurantList,
        onBack: () {
          this._refreshRestaurantList();
        });
    }
  }

  Widget buildBody(BuildContext context) {
    return _buildLoadedBody(context);
  }

  @override Widget build(BuildContext context) {
    Locale loc = Localizations.localeOf(context);
    String locStr = "${loc.languageCode}_${loc.countryCode}";
    initializeDateFormatting(locStr);

    var _debugButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Icon(CupertinoIcons.gear_big),
      onPressed: () => _showDebugPage(context));

    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Icon(CupertinoIcons.add),
      onPressed: () => _showAddRestaurantPage(context));

    var _navigationBar = CupertinoNavigationBar(
      middle: Text(widget.title),
      leading: this._debugMode ? _debugButton : null,
      trailing: _addButton,
    );

    if(this._loading || this._firstRunCompleted != true) {
      return LoadingPage();
    } else {
      return CupertinoPageScaffold(
        navigationBar: _navigationBar,
        child: buildBody(context),
      );
    }
  }
}
