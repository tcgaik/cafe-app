/*import 'package:flutter/material.dart';
import 'chart_widget.dart';
import 'chart_page.dart';
import 'database_helper.dart';
import 'user.dart';

enum ChartId {
  UserAllTime,
  UserMostActive,
}

class ChartWidgetFactory {
  final int rid;

  ChartWidgetFactory({@required this.rid});

  ChartWidget create(ChartId id) {
    switch(id) {
      case ChartId.UserAllTime:
        return ChartWidget(
          title: "Best player (all time)",
          subtitle: "Sum of all points received by each player",
          loadData: () async {
            var users = await DatabaseHelper().loadActiveUsersForRestaurantId(this.rid);
            var lst = List<ChartData>();
            for(User u in users) {
              var score = await DatabaseHelper().getScoreSumForUserId(u.id);
              lst.add(ChartData(u.name, score));
            }

            lst.sort((a, b) => b.points - a.points);
            return lst;
          });
      case ChartId.UserMostActive:
        return ChartWidget(
          title: "Most active player (all time)",
          subtitle: "Numbers of festivals attended for each player",
          loadData: () async {
            var users = await DatabaseHelper().loadActiveUsersForRestaurantId(this.rid);
            var lst = List<ChartData>();
            for(User u in users) {
              var count = await DatabaseHelper().countNumberOfFestivalsForUserId(u.id);
              lst.add(ChartData(u.name, count));
            }

            lst.sort((a, b) => b.points - a.points);
            return lst;
          });
      default:
        return null;
    }
  }
}

class SelectChartsPage extends StatefulWidget {
  final String set;
  final int rid;
  final int fid;

  SelectChartsPage({@required this.set, this.rid, this.fid});

  @override
  _SelectChartsPageState createState() => _SelectChartsPageState();
}

class _SelectChartsPageState extends State<SelectChartsPage> {
  ChartWidgetFactory fact;

  @override
  void initState() {
    super.initState();
    this.fact = ChartWidgetFactory(rid: this.widget.rid);
  }

  void _navigateToChart(ChartWidget chartWidget) {
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => ChartPage(chartWidget: chartWidget)
    ));
  }

  Widget _buildChartSelectionListView(List<ChartId> ids) {
    var titles = ids.map((i) {
      return this.fact.create(i);
    });

    return ListView(children: titles.map((chartObj) {
      return ListTile(
        title: Text(chartObj.title),
        onTap: () {
          _navigateToChart(chartObj);
        },
      );
    }).toList());
  }

  Widget _chartSetToChartList(String set) {
    final List<ChartId> arr = [];
    switch(set) {
      case "user":
        arr.add(ChartId.UserAllTime);
        arr.add(ChartId.UserMostActive);
        break;
      case "festival":
        break;
    }

    return _buildChartSelectionListView(arr);
  }

  Widget _buildChartList() {
    return _chartSetToChartList(this.widget.set);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Select chart")
      ),
      body: _buildChartList()
    );
  }
}*/
