import 'package:flutter/material.dart';

class ImageBox extends StatelessWidget {
  ImageBox({Key key, this.imagePath, this.width, this.height}) : super(key: key);

  final String imagePath;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: width,
        height: height,
        child: DecoratedBox(
            decoration: BoxDecoration(
                color: const Color(0x00000000),
                //border: Border.all(width: 0.0),
                image: DecorationImage(
                  image: ExactAssetImage(imagePath),
                  fit: BoxFit.contain,
                ))));
  }
}
