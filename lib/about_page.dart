import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:package_info/package_info.dart';

import 'package:cafe_app/strings.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/loading_page.dart';

class AboutPage extends StatefulWidget {
  final Township r;

  AboutPage({@required this.r});

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  StringLocalizations _;
  PackageInfo _package;

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((pi) => setState(() {
      this._package = pi;
    }));
  }

  @override
  Widget build(BuildContext context) {
    this._ = StringLocalizations.of(this.context);

    var _navBar = CupertinoNavigationBar(
      middle: Text(_.aboutPageTitle),
      previousPageTitle: this.widget.r.name,
    );

    if(this._package == null) {
      return LoadingPage();
    } else {
      return CupertinoPageScaffold(
        navigationBar: _navBar,
        child: SafeArea(child: this._buildBody())
      );
    }
  }

  void _launchURL(String url) async {
    bool ok = await canLaunch(url);
    if(ok) {
      debugPrint("launch");
      launch(url);
    }
  }

  Widget _buildBody() {
    var _items = <AbstractPropertyWidget>[
      TextPropertyWidget.from(i18n(this.context).aboutAppName, "v${this._package.version}",
        boldName: true, underlinedName: true),

      TextPropertyWidget.from(_.aboutWrittenBy, "Grzegorz Antoniak",
        boldName: true),

      TextPropertyWidget.from(_.aboutDedicatedTo, "Valergia",
        boldName: true),

      TextPropertyWidget.from(_.aboutTranslationBy, _.aboutTranslationAuthor, boldName: true),

      LinePropertyWidget.from(""),

      CustomPropertyWidget.from(Text(_.aboutOpenBlog),
          LinkWidget(),
          onTapped: (_) => _launchURL("http://anadoxin.org/blog")),

      CustomPropertyWidget.from(Text(_.aboutOpenSource),
          LinkWidget(),
          onTapped: (_) => _launchURL("https://gitlab.com/antekone/cafe-app")),

      LinePropertyWidget.from(""),

      LinePropertyWidget.from(_.aboutThirdpartyStuff),

      CustomPropertyWidget.from(Text("CircleIcons home page"),
        LinkWidget(),
        onTapped: (_) => _launchURL("https://www.elegantthemes.com/blog/freebie-of-the-week/beautiful-flat-icons-for-free")),

      CustomPropertyWidget.from(Text("FontAwesome"),
          LinkWidget(),
          onTapped: (_) => _launchURL("https://pub.dartlang.org/packages/font_awesome_flutter")),

      CustomPropertyWidget.from(Text("sqflite"),
        LinkWidget(),
        onTapped: (_) => _launchURL("https://pub.dartlang.org/packages/sqflite")),

      LinePropertyWidget.from(""),
      LinePropertyWidget.from(_.aboutWrittenInFlutter),
      LinePropertyWidget.from("https://flutter.io"),
      LinePropertyWidget.from(""),
    ];

    return PropertyListView(
      drawLines: false,
      items: _items
    );
  }
}

class LinkWidget extends StatelessWidget {
  LinkWidget();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(""),
        Icon(FontAwesomeIcons.externalLinkAlt, size: 16, color: CupertinoColors.inactiveGray),
      ]
    );
  }
}

