import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:cafe_app/text_input_alert_dialog.dart';
import 'package:cafe_app/strings.dart';

const double _kPickerSheetHeight = 216.0;
const double _kPickerItemHeight = 32.0;

typedef CustomValidator<T> = bool Function(T newValue);

typedef SetCallback<T> = void Function(T newValue);
typedef TapCallback<T> = void Function(T newValue);
typedef SuffixCallback<T> = String Function(T value);
typedef GetCallback<T> = T Function();

typedef ChildHookCallback = Widget Function(Widget w);

mixin ComboInner {
  String getValueString();
}

abstract class AbstractPropertyWidget {
  List<Widget> buildItems();
  bool isCentered() => false;

  void onTap(BuildContext context) {
  }

  Widget childHook(Widget w) {
    return w;
  }
}

Widget _buildBottomPicker(Widget picker) {
  return Container(
    height: _kPickerSheetHeight,
    padding: const EdgeInsets.only(top: 6.0),
    color: CupertinoColors.white,
    child: DefaultTextStyle(
      style: const TextStyle(
        color: CupertinoColors.black,
        fontSize: 22.0,
      ),
      child: GestureDetector(
        // Blocks taps from propagating to the modal sheet and popping.
        onTap: () {},
        child: SafeArea(
          top: false,
          child: picker,
        ),
      ),
    ),
  );
}

class LinePropertyWidget extends AbstractPropertyWidget {
  final String line;

  LinePropertyWidget.from(this.line);
  LinePropertyWidget({@required this.line});

  @override
  bool isCentered() => true;

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildLine()];
  }

  Widget buildLine() {
    return Text(this.line,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontWeight: FontWeight.bold
      ));
  }
}

class ComboPropertyWidget<Inner extends ComboInner> extends AbstractPropertyWidget {
  final String name;
  final SetCallback<int> stateUpdate;
  List<Inner> objects;
  int currentObject;
  BuildContext context;

  ComboPropertyWidget.from(this.context, this.name, this.objects, {
    this.currentObject = -1,
    this.stateUpdate,
  });

  ComboPropertyWidget({
    @required this.name,
    @required this.objects,
    this.currentObject = -1,
    this.stateUpdate,
  });

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    return Text(this.name);
  }

  Widget buildValue() {
    var _text = (
      this.objects == null ||
      this.currentObject == -1 ||
      this.currentObject >= this.objects.length
    ) ? i18n(this.context).festNoPlayersAvailable : this.objects[this.currentObject].getValueString();

    return Text(_text,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    final FixedExtentScrollController scrollController =
      FixedExtentScrollController(initialItem: this.currentObject == -1 ? 0 : this.currentObject);

    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(
          CupertinoPicker(
            scrollController: scrollController,
            itemExtent: _kPickerItemHeight,
            backgroundColor: CupertinoColors.white,
            onSelectedItemChanged: (int index) {
              if(this.stateUpdate != null) {
                this.stateUpdate(index);
              }
            },
            children: List<Widget>.generate(this.objects.length, (int index) {
              return Center(child:
                Text(this.objects[index].getValueString()),
              );
            }),
          ),
        );
      },
    );
  }
}

class DatePropertyWidget extends AbstractPropertyWidget {
  final String name;
  final SetCallback<DateTime> setCallback;
  DateTime value;

  DatePropertyWidget.from(this.name, this.value, {this.setCallback});
  DatePropertyWidget({this.name, this.value, this.setCallback});

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    return Text(this.name);
  }

  Widget buildValue() {
    var _dateText = DateFormat.yMMMMd().format(this.value);
    return Text(_dateText,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(
          CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: this.value,
            onDateTimeChanged: (DateTime newDateTime) {
              this.value = newDateTime;
              if(this.setCallback != null) {
                this.setCallback(this.value);
              }
            },
          ),
        );
      },
    );
  }
}

class BooleanPropertyWidget extends AbstractPropertyWidget {
  final String name;
  final Widget child;
  final SetCallback<bool> setCallback;
  final GetCallback<bool> getCallback;
  final bool defaultValue;

  BooleanPropertyWidget({this.name,
    this.child,
    this.defaultValue,
    this.getCallback,
    this.setCallback});

  @override
  List<Widget> buildItems() {
    return [this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    if(this.child == null) {
      return Text(this.name);
    } else {
      return child;
    }
  }

  Widget buildValue() {
    return CupertinoSwitch(
      onChanged: (bool v) => this.setCallback(v),
      value: this.defaultValue,
    );
  }

  @override onTap(BuildContext ctx) {
    this.setCallback(!this.getCallback());
  }
}

class NumericPropertyWidget extends AbstractPropertyWidget {
  final NumberFormat _fmt = NumberFormat.decimalPattern();
  final String name;
  final SuffixCallback<int> suffix;
  final String editTitle;
  final String editBody;
  final CustomValidator<int> customValidator;
  final SetCallback<int> setCallback;
  final ChildHookCallback mutateChild;
  final TapCallback<int> onTapped;
  int value;

  NumericPropertyWidget.from(this.name, this.value, {
    this.setCallback,
    this.suffix,
    this.editTitle,
    this.editBody,
    this.mutateChild,
    this.onTapped,
    this.customValidator,
  });

  NumericPropertyWidget({
    @required this.name,
    @required this.value,
    this.setCallback,
    this.suffix,
    this.editTitle,
    this.editBody,
    this.mutateChild,
    this.onTapped,
    this.customValidator,
  });

  @override
  Widget childHook(Widget w) {
    if(this.mutateChild != null) {
      return this.mutateChild(w);
    } else {
      return super.childHook(w);
    }
  }

  @override
  List<Widget> buildItems() {
    return [this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    return Text(this.name);
  }

  String _formatValue() {
    return this._fmt.format(this.value);
  }

  Widget buildValue() {
    var _numText = this._formatValue();
    if(this.suffix != null) {
      _numText += " ";
      _numText += this.suffix(this.value);
    }

    return Text(_numText,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    if(this.onTapped != null) {
      // User tap handler.
      this.onTapped(this.value);
    } else {
      // Default tap handler for Numeric type.
      TextInputAlertDialog.show(context,
        title: this.editTitle ?? "",
        body: this.editBody ?? "",
        numericKeyboard: true,
        onOK: (buf) => this.setCallback(int.parse(buf)),
        textValidator: (buf) {
          var num = int.tryParse(buf);
          if(num == null)
            return false;

          if(this.customValidator != null) {
            return this.customValidator(num);
          } else {
            return true;
          }
        },
        initialText: "",
      );
    }
  }
}

class CustomPropertyWidget extends AbstractPropertyWidget {
  final Widget name;
  final Widget value;
  final TapCallback<void> onTapped;

  CustomPropertyWidget.from(this.name, this.value, {this.onTapped});
  CustomPropertyWidget({this.name, this.value, this.onTapped});

  @override
  List<Widget> buildItems() {
    return <Widget>[this.name, this.value];
  }

  @override
  void onTap(BuildContext context) {
    if(this.onTapped != null) {
      this.onTapped(null);
    }
  }
}

class TextPropertyWidget extends AbstractPropertyWidget {
  final String name;
  final String value;
  final TapCallback<String> onTapped;
  final bool boldName;
  final bool underlinedName;

  TextPropertyWidget.from(this.name, this.value, {
    this.onTapped,
    this.boldName = false,
    this.underlinedName = false,
  });

  TextPropertyWidget({
    @required this.name,
    @required this.value,
    this.onTapped,
    this.boldName = false,
    this.underlinedName = false,
  });

  @override
  List<Widget> buildItems() {
    return <Widget>[this.buildName(), this.buildValue()];
  }

  Widget buildName() {
    var weight = this.boldName ? FontWeight.bold : FontWeight.normal;
    var decor = this.underlinedName ? TextDecoration.underline : TextDecoration.none;

    return Text(this.name,
      style: TextStyle(
        fontWeight: weight, decoration: decor
      )
    );
  }

  Widget buildValue() {
    return Text(this.value,
      style: TextStyle(color: CupertinoColors.inactiveGray));
  }

  @override
  void onTap(BuildContext context) {
    if(this.onTapped != null) {
      // User tap handler.
      this.onTapped(this.value);
    }
  }
}

class PropertyListView extends StatefulWidget {
  final List<AbstractPropertyWidget> items;
  final bool drawLines;

  PropertyListView({Key key, this.items, this.drawLines = true}): super(key: key);

  @override
  _PropertyListViewState createState() => _PropertyListViewState();
}

class _PropertyListViewState extends State<PropertyListView> {
  Widget _buildRow(AbstractPropertyWidget item) {
    var _borderDecoration = this.widget.drawLines
      ? BoxDecoration(
          color: CupertinoColors.white,
          border: const Border(
            top: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
            bottom: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
          )
        )
      : BoxDecoration(
          color: CupertinoColors.white);

    var _container = Container(
      decoration: _borderDecoration,
      height: this.widget.drawLines ? 44.0 : 34.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: SafeArea(
          top: false,
          bottom: false,
          child: Row(
            mainAxisAlignment: item.isCentered() ?
              MainAxisAlignment.center :
              MainAxisAlignment.spaceBetween,
            children: item.buildItems(),
          ),
        ),
      ),
    );

    var _mutated = item.childHook(_container);
    return _mutated;
  }

  @override
  Widget build(BuildContext context) {
    var _lv = ListView(
      children: <Widget>[] + this.widget.items
        .where((AbstractPropertyWidget i) => i != null)
        .map((AbstractPropertyWidget item)
      {
        var _child = this._buildRow(item);
        var _detector = GestureDetector(
          child: _child,
          behavior: HitTestBehavior.opaque,
          onTap: () => item.onTap(context),
        );

        return _detector;
      }).toList(),
    );

    return _lv;
  }
}
