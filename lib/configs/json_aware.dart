abstract class JsonAware {
  Map<String, dynamic> toJson();
}