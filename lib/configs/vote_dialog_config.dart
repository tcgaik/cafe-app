import 'package:cafe_app/configs/json_aware.dart';
import 'package:json_annotation/json_annotation.dart';
part 'vote_dialog_config.g.dart';

@JsonSerializable()

class VoteDialogConfig implements JsonAware {
  VoteDialogConfig(this.lastDisplayDate, this.neverDisplay);

  @JsonKey(required: false)
  DateTime lastDisplayDate;

  @JsonKey(required: false)
  bool neverDisplay;

  @JsonKey(required: false)
  int lastVersionCode = 0;

  factory VoteDialogConfig.fromJson(Map<String, dynamic> json) =>
      _$VoteDialogConfigFromJson(json);

  @override
  Map<String, dynamic> toJson() =>
      _$VoteDialogConfigToJson(this);

  @override String toString() {
    return "VoteDialogConfig[lastDisplayDate=${this.lastDisplayDate},neverDisplay=${this.neverDisplay}]";
  }
}

