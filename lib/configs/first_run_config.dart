import 'package:cafe_app/configs/json_aware.dart';
import 'package:json_annotation/json_annotation.dart';
part 'first_run_config.g.dart';

@JsonSerializable()

class FirstRunConfig implements JsonAware {
  FirstRunConfig(this.firstRunDate);

  factory FirstRunConfig.now() {
    return FirstRunConfig(DateTime.now());
  }

  @JsonKey(required: false)
  DateTime firstRunDate;

  factory FirstRunConfig.fromJson(Map<String, dynamic> json) =>
    _$FirstRunConfigFromJson(json);

  @override
  Map<String, dynamic> toJson() =>
    _$FirstRunConfigToJson(this);

  @override String toString() {
    return "FirstRunConfig[firstRunDate=${this.firstRunDate}]";
  }
}

