import 'package:async/async.dart';
import 'package:cafe_app/configs/first_run_config.dart';
import 'package:cafe_app/configs/vote_dialog_config.dart';
import 'package:cafe_app/configs/json_aware.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/debug_utils.dart' as DebugUtils;
import 'dart:convert';

class VoteConfig {
  int voteDialogTimesShown = 0;
  bool voteDialogShouldShow = true;
}

class ConfigLoader {
  static const int FIRST_RUN_CONFIG_ID = 1;
  static const int VOTE_DIALOG_CONFIG_ID = 2;

  static Future<Result<Map<String, dynamic>>> getConfigObject(int id) async {
    var db = DatabaseHelper();
    var configText = await db.getConfigTextById(id);
    if(configText.isValue) {
      try {
        return Result.value(jsonDecode(configText.asValue.value));
      } on FormatException catch(_) {
        return Result.error("invalid json format");
      }
    } else {
      return Result.error(configText.asError.toString()); // return error
    }
  }

  static Future<bool> saveConfigObject(int id, JsonAware jsonObject) async {
    print("Saving config object id=$id, jsonObject=${jsonObject.toJson()}");
    var db = DatabaseHelper();
    var configText = jsonEncode(jsonObject.toJson());
    if(configText == null) {
      return false;
    }

    return db.setConfigTextById(id, configText);
  }

  static Future<Result<T>> getTypedConfigObject<T>(int id, Map<String, dynamic> json) async {
    DebugUtils.run(() {
      print(",---- Debug dump of initial load of config object $id:");
      json.forEach((k, v) { print("| $k=$v"); });
      print("`----");
    });

    if(id == FIRST_RUN_CONFIG_ID) {
      return Result.value(FirstRunConfig.fromJson(json) as T);
    } else if(id == VOTE_DIALOG_CONFIG_ID) {
      return Result.value(VoteDialogConfig.fromJson(json) as T);
    } else {
      DebugUtils.run(() => throw "Unsupported getTypedConfigObject type");
      return Result.error("Unsupported getTypedConfigObject type");
    }
  }

  static Future<Result<FirstRunConfig>> getFirstRunConfig() async {
    var configObjectResult = await getConfigObject(FIRST_RUN_CONFIG_ID);
    if(configObjectResult.isValue) {
      return getTypedConfigObject<FirstRunConfig>(FIRST_RUN_CONFIG_ID,
          configObjectResult.asValue.value);
    } else {
      return Result.error(configObjectResult.asError.toString());
    }
  }

  static Future<Result<VoteDialogConfig>> getVoteDialogConfig() async {
    var configObjectResult = await getConfigObject(VOTE_DIALOG_CONFIG_ID);
    if(configObjectResult.isValue) {
      return getTypedConfigObject<VoteDialogConfig>(VOTE_DIALOG_CONFIG_ID,
          configObjectResult.asValue.value);
    } else {
      return Result.error(configObjectResult.asError.toString());
    }
  }

  static Future<bool> saveFirstRunConfig(FirstRunConfig config) =>
    saveConfigObject(FIRST_RUN_CONFIG_ID, config);

  static Future<bool> saveVoteDialogConfig(VoteDialogConfig config) =>
    saveConfigObject(VOTE_DIALOG_CONFIG_ID, config);
}