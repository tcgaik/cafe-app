class User {
  String _name;
  int _id;
  int _restaurantId;
  int _disabled;

  User(this._id, this._restaurantId, this._name, this._disabled);

  User.map(dynamic object) {
    this._id = object['id'];
    this._name = object['name'];
    this._restaurantId = object['rid'];
    this._disabled = object['disabled'];
  }

  String get name => this._name;
  int get id => this._id;
  int get restaurantId => this._restaurantId;
  bool get disabled => this._disabled == 1;

  Map<String, dynamic> toMap() {
    var m = Map<String, dynamic>();
    m['id'] = this._id;
    m['name'] = this._name;
    m['rid'] = this._restaurantId;
    m['disabled'] = this._disabled;
    return m;
  }

  Map<String, dynamic> toMapNoId() {
    var m = this.toMap();
    m.remove('id');
    return m;
  }

  @override
  String toString() {
    return "User[id=${this.id}, name=${this.name}]";
  }
}
