// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  static m0(value) => "${Intl.plural(value, zero: 'diamonds', one: 'diamond', two: 'diamonds', many: 'diamonds', other: 'diamonds')}";

  static m1(value) => "${Intl.plural(value, zero: 'points', one: 'point', two: 'points', many: 'points', other: 'points')}";

  static m2(value) => "${Intl.plural(value, zero: 'quests', one: 'quest', two: 'quests', many: 'quests', other: 'quests')}";

  static m3(value) => "${Intl.plural(value, zero: 'rubies', one: 'ruby', two: 'rubies', many: 'rubies', other: 'rubies')}";

  static m4(value) => "${Intl.plural(value, zero: 'trophies', one: 'trophy', two: 'trophies', many: 'trophies', other: 'trophies')}";

  static m5(minValue, maxValue) => "How many quests has been completed by this user?\n\nEnter a value between ${minValue} and ${maxValue}.";

  static m6(minValue, maxValue) => "How many trophies did this user get?\n\nEnter a value between ${minValue} and ${maxValue}.";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "aboutAppName" : MessageLookupByLibrary.simpleMessage("MyCafe Calculator"),
    "aboutDedicatedTo" : MessageLookupByLibrary.simpleMessage("Dedicated to"),
    "aboutOpenBlog" : MessageLookupByLibrary.simpleMessage("Open blog"),
    "aboutOpenSource" : MessageLookupByLibrary.simpleMessage("Open source code"),
    "aboutPageTitle" : MessageLookupByLibrary.simpleMessage("About"),
    "aboutThirdpartyStuff" : MessageLookupByLibrary.simpleMessage("Thirdparty stuff:"),
    "aboutTranslationAuthor" : MessageLookupByLibrary.simpleMessage("Grzegorz"),
    "aboutTranslationBy" : MessageLookupByLibrary.simpleMessage("Translation by"),
    "aboutWrittenBy" : MessageLookupByLibrary.simpleMessage("Written by"),
    "aboutWrittenInFlutter" : MessageLookupByLibrary.simpleMessage("Written in Flutter!"),
    "emailFeedbackSubject" : MessageLookupByLibrary.simpleMessage("MyCafe Calculator feedback"),
    "emailNegativeFeedbackBody" : MessageLookupByLibrary.simpleMessage("Hi. I don\'t like your MyCafe Calculator. Here\'s why:\n\n"),
    "festAddPlayer" : MessageLookupByLibrary.simpleMessage("Add player to festival"),
    "festBackButtonTitle" : MessageLookupByLibrary.simpleMessage("Festival"),
    "festCheckYourPoints" : MessageLookupByLibrary.simpleMessage("Make sure you\'ve distributed your points correctly."),
    "festChoose" : MessageLookupByLibrary.simpleMessage("Choose:"),
    "festChooseProperties" : MessageLookupByLibrary.simpleMessage("Choose properties:"),
    "festControlInstructions" : MessageLookupByLibrary.simpleMessage("Slide left to remove. Tap to edit."),
    "festDate" : MessageLookupByLibrary.simpleMessage("Date"),
    "festDiamondCount" : MessageLookupByLibrary.simpleMessage("Diamond count"),
    "festDiamondReceivedTitle" : MessageLookupByLibrary.simpleMessage("How many diamonds have you received?"),
    "festDiamonds" : MessageLookupByLibrary.simpleMessage("Diamonds"),
    "festEditPageTitle" : MessageLookupByLibrary.simpleMessage("Edit festival"),
    "festNeedMorePlayers" : MessageLookupByLibrary.simpleMessage("You need to add more players to this township!"),
    "festNoFestivalsAddedYet" : MessageLookupByLibrary.simpleMessage("No festivals added yet."),
    "festNoPlayersAddedYet" : MessageLookupByLibrary.simpleMessage("Add players to this festival!\n\nTap the \'Add Player\' button in the top right corner."),
    "festNoPlayersAvailable" : MessageLookupByLibrary.simpleMessage("(none)"),
    "festOnlyTrophies" : MessageLookupByLibrary.simpleMessage("Only trophies"),
    "festPlayerList" : MessageLookupByLibrary.simpleMessage("Player list.\n\nTap to toggle."),
    "festPlayerRemoved" : MessageLookupByLibrary.simpleMessage("(inactive)"),
    "festQuestCount" : MessageLookupByLibrary.simpleMessage("Quest count"),
    "festQuestReceivedTitle" : MessageLookupByLibrary.simpleMessage("How many quests were completed?"),
    "festQuests" : MessageLookupByLibrary.simpleMessage("Quests"),
    "festRubies" : MessageLookupByLibrary.simpleMessage("Rubies"),
    "festRubyCount" : MessageLookupByLibrary.simpleMessage("Ruby count"),
    "festRubyReceivedTitle" : MessageLookupByLibrary.simpleMessage("How many rubies have you received?"),
    "festSelectPlayer" : MessageLookupByLibrary.simpleMessage("Select player by tapping his/her name."),
    "festSetQuestPointsTitle" : MessageLookupByLibrary.simpleMessage("Set quest points"),
    "festSetTrophyPointsTitle" : MessageLookupByLibrary.simpleMessage("Set trophy points"),
    "festSummary" : MessageLookupByLibrary.simpleMessage("What\'s left?"),
    "festTrophiesReceivedTitle" : MessageLookupByLibrary.simpleMessage("How many trophies have you received?"),
    "festTrophyCount" : MessageLookupByLibrary.simpleMessage("Trophy count"),
    "festUseQuests" : MessageLookupByLibrary.simpleMessage("Add quest count"),
    "globAdd" : MessageLookupByLibrary.simpleMessage("Add"),
    "globBack" : MessageLookupByLibrary.simpleMessage("Back"),
    "globCancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "globCreate" : MessageLookupByLibrary.simpleMessage("Create"),
    "globDiamondSuffix" : m0,
    "globDoNothing" : MessageLookupByLibrary.simpleMessage("Do nothing"),
    "globHello" : MessageLookupByLibrary.simpleMessage("Hello!"),
    "globLoading" : MessageLookupByLibrary.simpleMessage("Loading..."),
    "globOk" : MessageLookupByLibrary.simpleMessage("OK"),
    "globPleaseWait" : MessageLookupByLibrary.simpleMessage("Please wait..."),
    "globPointSuffix" : m1,
    "globQuestPoints" : MessageLookupByLibrary.simpleMessage("Quest points"),
    "globQuestSuffix" : m2,
    "globQuestion" : MessageLookupByLibrary.simpleMessage("Question"),
    "globRemove" : MessageLookupByLibrary.simpleMessage("Remove"),
    "globRubySuffix" : m3,
    "globSorry" : MessageLookupByLibrary.simpleMessage("Sorry!"),
    "globThankYou" : MessageLookupByLibrary.simpleMessage("Thank you!"),
    "globTrophyPoints" : MessageLookupByLibrary.simpleMessage("Trophy points"),
    "globTrophySuffix" : m4,
    "globVoteDialogQuestion" : MessageLookupByLibrary.simpleMessage("Do you think this app is useful?"),
    "globVoteNoAnswer" : MessageLookupByLibrary.simpleMessage("I\'m sorry the app doesn\'t perform as you\'d like. If you have any suggestions, you can contact the author directly! Do you want to compose an e-mail now?"),
    "globVoteYesAnswer" : MessageLookupByLibrary.simpleMessage("Would you like to rate the app in Play Store, so that other people will also know about it?"),
    "globWarning" : MessageLookupByLibrary.simpleMessage("Warning!"),
    "mainAbout" : MessageLookupByLibrary.simpleMessage("About this app"),
    "mainAddNewPlayer" : MessageLookupByLibrary.simpleMessage("Add a new player"),
    "mainFestivalHistory" : MessageLookupByLibrary.simpleMessage("Festival history"),
    "mainFestivals" : MessageLookupByLibrary.simpleMessage("Festivals"),
    "mainManagePlayers" : MessageLookupByLibrary.simpleMessage("Manage players"),
    "mainManagement" : MessageLookupByLibrary.simpleMessage("Management"),
    "mainNewFestival" : MessageLookupByLibrary.simpleMessage("New festival"),
    "mainPlayers" : MessageLookupByLibrary.simpleMessage("Players"),
    "mainRemoveTownshipBody" : MessageLookupByLibrary.simpleMessage("Do you want to permanently remove this township, all its users and festivals?"),
    "mainRemoveTownshipTitle" : MessageLookupByLibrary.simpleMessage("Remove this township"),
    "mainSelectOption" : MessageLookupByLibrary.simpleMessage("Please select desired option from the menu below."),
    "playerAdd" : MessageLookupByLibrary.simpleMessage("Add player"),
    "playerAddNew" : MessageLookupByLibrary.simpleMessage("Add a new player..."),
    "playerInstructionText" : MessageLookupByLibrary.simpleMessage("Swipe left to remove the player. Long tap to rename the player."),
    "playerName" : MessageLookupByLibrary.simpleMessage("Player name"),
    "playerNewNameBody" : MessageLookupByLibrary.simpleMessage("What is the name of this new player?"),
    "playerRename" : MessageLookupByLibrary.simpleMessage("Rename"),
    "playerRenameBody" : MessageLookupByLibrary.simpleMessage("What is the new name of this player?"),
    "playerRenameTitle" : MessageLookupByLibrary.simpleMessage("Rename player"),
    "setQuestPointsBody" : m5,
    "setTrophyPointsBody" : m6,
    "townAddBody" : MessageLookupByLibrary.simpleMessage("Enter the name of your new township.\n\nWhen finished, tap the \'Add\' button in the top right corner."),
    "townAddTitle" : MessageLookupByLibrary.simpleMessage("Add a township"),
    "townList" : MessageLookupByLibrary.simpleMessage("My townships"),
    "townListUpper" : MessageLookupByLibrary.simpleMessage("TOWNSHIP LIST"),
    "townNothingAddedYet" : MessageLookupByLibrary.simpleMessage("You need to add a township!\n\nIn order to do this, please use the plus icon in the top right corner and then follow the instructions."),
    "townSwipeLeftToRemove" : MessageLookupByLibrary.simpleMessage("(swipe left to remove)"),
    "voteEMail" : MessageLookupByLibrary.simpleMessage("E-Mail"),
    "voteNo" : MessageLookupByLibrary.simpleMessage("No 😟"),
    "voteRateIt" : MessageLookupByLibrary.simpleMessage("Rate it"),
    "voteYes" : MessageLookupByLibrary.simpleMessage("Yes! 😀")
  };
}
