// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'es';

  static m0(value) => "${Intl.plural(value, zero: 'Diamantes', one: 'Diamante', two: 'Diamantes', many: 'Diamantes', other: 'Diamantes')}";

  static m1(value) => "${Intl.plural(value, zero: 'Puntos', one: 'Punto', two: 'Puntos', many: 'Puntos', other: 'Puntos')}";

  static m2(value) => "${Intl.plural(value, zero: 'Tareas', one: 'Tarea', two: 'Tareas', many: 'Tareas', other: 'Tareas')}";

  static m3(value) => "${Intl.plural(value, zero: 'Rubíes', one: 'Rubí', two: 'Rubíes', many: 'Rubíes', other: 'Rubíes')}";

  static m4(value) => "${Intl.plural(value, zero: 'Trofeos', one: 'Trofeo', two: 'Trofeos', many: 'Trofeos', other: 'Trofeos')}";

  static m5(minValue, maxValue) => "Cuantas tareas completó este jugador?\n\nIngrese un valor entre ${minValue} y ${maxValue}.";

  static m6(minValue, maxValue) => "Cuantos trofeos obtuvo este jugador?\n\nIngrese un valor entre ${minValue} y ${maxValue}.";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "aboutAppName" : MessageLookupByLibrary.simpleMessage("MyCafe Calculadora"),
    "aboutDedicatedTo" : MessageLookupByLibrary.simpleMessage("Dedicado a"),
    "aboutOpenBlog" : MessageLookupByLibrary.simpleMessage("Abrir Blog"),
    "aboutOpenSource" : MessageLookupByLibrary.simpleMessage("Abrir código fuente"),
    "aboutPageTitle" : MessageLookupByLibrary.simpleMessage("Información"),
    "aboutThirdpartyStuff" : MessageLookupByLibrary.simpleMessage("Información extra"),
    "aboutTranslationAuthor" : MessageLookupByLibrary.simpleMessage("Florencia Castillo"),
    "aboutTranslationBy" : MessageLookupByLibrary.simpleMessage("Traducido por"),
    "aboutWrittenBy" : MessageLookupByLibrary.simpleMessage("Escrito por"),
    "aboutWrittenInFlutter" : MessageLookupByLibrary.simpleMessage("Escrito en Flutter!"),
    "emailFeedbackSubject" : MessageLookupByLibrary.simpleMessage("MyCafe Calculadora opiniones"),
    "emailNegativeFeedbackBody" : MessageLookupByLibrary.simpleMessage("Hola! No me ha gustado MyCafe Calculadora. Estas son las razones:"),
    "festAddPlayer" : MessageLookupByLibrary.simpleMessage("Agregar jugador al festival"),
    "festBackButtonTitle" : MessageLookupByLibrary.simpleMessage("Festival"),
    "festCheckYourPoints" : MessageLookupByLibrary.simpleMessage("Asegúrate que haz distribuido tus puntos correctamente."),
    "festChoose" : MessageLookupByLibrary.simpleMessage("Elegir"),
    "festChooseProperties" : MessageLookupByLibrary.simpleMessage("Seleccionar propiedades"),
    "festControlInstructions" : MessageLookupByLibrary.simpleMessage("Deslizar hacia la izq. para eliminar. Toca para editar."),
    "festDate" : MessageLookupByLibrary.simpleMessage("Fecha"),
    "festDiamondCount" : MessageLookupByLibrary.simpleMessage("Diamantes recibidos"),
    "festDiamondReceivedTitle" : MessageLookupByLibrary.simpleMessage("Cuántos diamantes ha recibido?"),
    "festDiamonds" : MessageLookupByLibrary.simpleMessage("Diamantes"),
    "festEditPageTitle" : MessageLookupByLibrary.simpleMessage("Editar festival"),
    "festNeedMorePlayers" : MessageLookupByLibrary.simpleMessage("Necesitas agregar mas jugadores a este pueblo!"),
    "festNoFestivalsAddedYet" : MessageLookupByLibrary.simpleMessage("No se ha agregado ningún festival aún"),
    "festNoPlayersAddedYet" : MessageLookupByLibrary.simpleMessage("Agregar jugadores a este festival!\n\nPresiona la opción \"Agregar Jugador\" en la esquina superior derecha de la pantalla."),
    "festNoPlayersAvailable" : MessageLookupByLibrary.simpleMessage("(ninguno)"),
    "festOnlyTrophies" : MessageLookupByLibrary.simpleMessage("Solo trofeos"),
    "festPlayerList" : MessageLookupByLibrary.simpleMessage("Lista de jugadores.\n\nToque para alternar."),
    "festPlayerRemoved" : MessageLookupByLibrary.simpleMessage("(inactivo)"),
    "festQuestCount" : MessageLookupByLibrary.simpleMessage("Tareas realizadas"),
    "festQuestReceivedTitle" : MessageLookupByLibrary.simpleMessage("Cuántas tareas han sido completadas?"),
    "festQuests" : MessageLookupByLibrary.simpleMessage("Tareas"),
    "festRubies" : MessageLookupByLibrary.simpleMessage("Rubies"),
    "festRubyCount" : MessageLookupByLibrary.simpleMessage("Rubies recibidos"),
    "festRubyReceivedTitle" : MessageLookupByLibrary.simpleMessage("Cuántos rubíes ha recibido?"),
    "festSelectPlayer" : MessageLookupByLibrary.simpleMessage("Selecciona el jugador tocando su nombre."),
    "festSetQuestPointsTitle" : MessageLookupByLibrary.simpleMessage("Agregar puntos de tareas"),
    "festSetTrophyPointsTitle" : MessageLookupByLibrary.simpleMessage("Agregar puntos de trofeos"),
    "festSummary" : MessageLookupByLibrary.simpleMessage("Qué queda?"),
    "festTrophiesReceivedTitle" : MessageLookupByLibrary.simpleMessage("Cuántos trofeos ha recibido?"),
    "festTrophyCount" : MessageLookupByLibrary.simpleMessage("Trofeos ganados"),
    "festUseQuests" : MessageLookupByLibrary.simpleMessage("Agregar recuento de tareas"),
    "globAdd" : MessageLookupByLibrary.simpleMessage("Agregar"),
    "globBack" : MessageLookupByLibrary.simpleMessage("Atrás"),
    "globCancel" : MessageLookupByLibrary.simpleMessage("Cancelar"),
    "globCreate" : MessageLookupByLibrary.simpleMessage("Crear"),
    "globDiamondSuffix" : m0,
    "globDoNothing" : MessageLookupByLibrary.simpleMessage("Cancelar acción"),
    "globHello" : MessageLookupByLibrary.simpleMessage("Hola!"),
    "globLoading" : MessageLookupByLibrary.simpleMessage("Cargando..."),
    "globOk" : MessageLookupByLibrary.simpleMessage("OK"),
    "globPleaseWait" : MessageLookupByLibrary.simpleMessage("Aguarde por favor..."),
    "globPointSuffix" : m1,
    "globQuestPoints" : MessageLookupByLibrary.simpleMessage("Tareas"),
    "globQuestSuffix" : m2,
    "globQuestion" : MessageLookupByLibrary.simpleMessage("Pregunta"),
    "globRemove" : MessageLookupByLibrary.simpleMessage("Eliminar"),
    "globRubySuffix" : m3,
    "globSorry" : MessageLookupByLibrary.simpleMessage("Disculpe!"),
    "globThankYou" : MessageLookupByLibrary.simpleMessage("Gracias!"),
    "globTrophyPoints" : MessageLookupByLibrary.simpleMessage("Trofeos"),
    "globTrophySuffix" : m4,
    "globVoteDialogQuestion" : MessageLookupByLibrary.simpleMessage("Crees que esta aplicación es útil?"),
    "globVoteNoAnswer" : MessageLookupByLibrary.simpleMessage("Disculpe que la app no funcione como le gustaría. Si tiene alguna sugerencia, puede contactarse directamente con el autor! Quiere escribir un mail ahora?"),
    "globVoteYesAnswer" : MessageLookupByLibrary.simpleMessage("Te gustaría calificar esta app en Play Store, así otras personas la conocen?"),
    "globWarning" : MessageLookupByLibrary.simpleMessage("Advertencia!"),
    "mainAbout" : MessageLookupByLibrary.simpleMessage("Acerca de la aplicación"),
    "mainAddNewPlayer" : MessageLookupByLibrary.simpleMessage("Agregar un nuevo jugador"),
    "mainFestivalHistory" : MessageLookupByLibrary.simpleMessage("Historial de festivales"),
    "mainFestivals" : MessageLookupByLibrary.simpleMessage("Festivales"),
    "mainManagePlayers" : MessageLookupByLibrary.simpleMessage("Administrar jugadores"),
    "mainManagement" : MessageLookupByLibrary.simpleMessage("Administración"),
    "mainNewFestival" : MessageLookupByLibrary.simpleMessage("Nuevo festival"),
    "mainPlayers" : MessageLookupByLibrary.simpleMessage("Jugadores"),
    "mainRemoveTownshipBody" : MessageLookupByLibrary.simpleMessage("Quieres eliminar permanentemente este pueblo, todos sus usuarios y festivales?"),
    "mainRemoveTownshipTitle" : MessageLookupByLibrary.simpleMessage("Elimina este pueblo"),
    "mainSelectOption" : MessageLookupByLibrary.simpleMessage("Por favor seleccione una opción del siguiente menú."),
    "playerAdd" : MessageLookupByLibrary.simpleMessage("Agregar jugador"),
    "playerAddNew" : MessageLookupByLibrary.simpleMessage("Agregar un nuevo jugador..."),
    "playerInstructionText" : MessageLookupByLibrary.simpleMessage("Deslizar hacia la izq. para eliminar jugador. Toque largo para editar jugador.\n"),
    "playerName" : MessageLookupByLibrary.simpleMessage("Nombre del jugador"),
    "playerNewNameBody" : MessageLookupByLibrary.simpleMessage("Cual es el nombre de este nuevo jugador?"),
    "playerRename" : MessageLookupByLibrary.simpleMessage("Renombrar"),
    "playerRenameBody" : MessageLookupByLibrary.simpleMessage("Cual es el nuevo nombre de este jugador?"),
    "playerRenameTitle" : MessageLookupByLibrary.simpleMessage("Renombrar jugador"),
    "setQuestPointsBody" : m5,
    "setTrophyPointsBody" : m6,
    "townAddBody" : MessageLookupByLibrary.simpleMessage("Ingrese el nombre de su Pueblo nuevo.\n\nCuando finalice, presione el botón de \"Agregar\" en la esquina superior derecha."),
    "townAddTitle" : MessageLookupByLibrary.simpleMessage("Agrega un Pueblo"),
    "townList" : MessageLookupByLibrary.simpleMessage("Mis Pueblos"),
    "townListUpper" : MessageLookupByLibrary.simpleMessage("LISTA DE PUEBLOS"),
    "townNothingAddedYet" : MessageLookupByLibrary.simpleMessage("Necesitas agregar un Pueblo!\n\nPara realizar esta acción, por favor pulse el ícono de agregar que se encuentra en la esquina superior derecha de la pantalla, y luego siga las instrucciones."),
    "townSwipeLeftToRemove" : MessageLookupByLibrary.simpleMessage("(deslizar hacia la izq. para eliminar)"),
    "voteEMail" : MessageLookupByLibrary.simpleMessage("E-Mail"),
    "voteNo" : MessageLookupByLibrary.simpleMessage("No 😟"),
    "voteRateIt" : MessageLookupByLibrary.simpleMessage("Califica"),
    "voteYes" : MessageLookupByLibrary.simpleMessage("Si! 😀")
  };
}
