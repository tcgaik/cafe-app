require 'json'
require 'net/http'
require 'base64'
require 'date'
require 'pp'

PROJECT_ID = 250407

class Creds
  attr_accessor :user, :pass, :tok
  def initialize(u, p, t)
    @user = u
    @pass = p
    @tok = t
  end
end

def log(s)
  caller = Thread.current.backtrace[2..][0]
  if caller =~ /(.*?):(.*?):/
    fname = $1
    lineno = $2.to_i
  else
    fname = "?"
    lineno = -1
  end

  print "[#{fname}"
  if lineno != -1
    print ":#{lineno}"
  end
  puts "] #{s}"
end

def parse_cred_file()
  path = File.join(ENV["PWD"], ".poeditor.json")
  object = JSON.parse(IO.readlines(path).join("\n"))
  Creds.new(object["username"].strip, object["password"].strip, object["api_token"].strip)
rescue
  nil
end

def query_language(uri, creds, lang)
  auth_info = Base64.strict_encode64 "#{creds.user}:#{creds.pass}"
  payload = URI.encode_www_form({ "api_token" => creds.tok, "id": PROJECT_ID, "language": lang })
  headers = { "Content-Type" => "application/x-www-form-urlencoded", "Authorization" => "Basic #{auth_info}" }
  ret = Net::HTTP.post uri, payload, headers
  if ret != nil and ret.code == "200"
    return JSON.parse(ret.read_body)
  else
    return nil
  end
end

def compile_terms(json, lang)
  arb = {}
  json.each do |term_obj|
    term_id = term_obj["term"]
    term_text = term_obj["translation"]["content"]
    if term_text.is_a? String
      # Replace special entities like $minValue -> {minValue}
      term_text.gsub!(/\$([a-zA-Z0-9]+)/, '{\1}')
    elsif term_text.is_a? Object
      # Replace plural forms with ARB serialized format
      if term_text.key? "one" and term_text.key? "other"
        one = term_text["one"]
        other = term_text["other"]
        few = term_text["few"] || other
        many = term_text["many"] || other
        term_text = "{value,plural, =0{#{many}}=1{#{one}}=2{#{few}}many{#{many}}other{#{other}}}"
      else
        log "Error: Invalid object in term #{term_id}! Failing."
        exit 1
      end
    else
      log "Error: Invalid object in term #{term_id}! Failing."
      exit 1
    end

    arb[term_id] = term_text
  end

  arb['@@locale'] = lang
  arb['@@last_modified'] = DateTime.now.rfc3339

  JSON.pretty_generate arb
end

def main(lang, output_filename)
  creds = parse_cred_file()
  if creds == nil
    log "Error: missing .poeditor.json or it's invalid!"
    exit 1
  end

  uri = URI("https://api.poeditor.com/v2/terms/list")

  log "Reading translation for language #{lang}..."
  json = query_language(uri, creds, lang)
  if json == nil
    log "Error: invalid response from poeditor.com"
    exit 1
  end

  if not (json.key? "result" and json.key? "response")
    log "Error: invalid json response from poeditor.com"
    exit 1
  end

  json = json["result"]["terms"]
  arb = compile_terms json, lang

  File.open(output_filename, 'wt') do |fw|
    fw.write(arb)
  end

  0
end

if ARGV == nil or ARGV.size != 2
  log "Syntax:"
  log ""
  log "    ruby pull-remote-strings.rb <lang_code> <output_file>"
  log ""
  log "Script will return 0 if everything is OK, other value is there will be"
  log "an error."
  exit 1
end

main ARGV[0], ARGV[1]
