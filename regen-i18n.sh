#!/usr/bin/env sh

# flutter packages pub run intl_translation:extract_to_arb --output-dir=lib/i18n lib/strings.dart

flutter packages pub run intl_translation:generate_from_arb --output-dir=lib/i18n --no-use-deferred-loading lib/strings.dart lib/i18n/intl_??.arb
