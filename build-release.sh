#!/usr/bin/env sh

rm build/app/outputs/apk/release/app-release.apk
flutter build apk --release
ls -la build/app/outputs/apk/release/app-release.apk
